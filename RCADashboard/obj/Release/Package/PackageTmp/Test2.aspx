﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Test2.aspx.cs" Inherits="RCADashboard.Test2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageTitlePlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="server">

<style>
/* The container */
.container {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 1px;      /*12*/
  cursor: pointer;
  /*font-size: 15px;*/          /*22*/
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default checkbox */
.container input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 15px;           /*25*/
  width: 15px;            /*25*/
  /*background-color: #eee;*/
  background-color:chocolate;
}

/* On mouse-over, add a grey background color */
.container:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container input:checked ~ .checkmark {
  /*background-color: #2196F3;*/
  background-color:limegreen;

}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.container input:checked ~ .checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.container .checkmark:after {
  left: 5px;          /*9*/
  top: 1px;           /*3*/
  width: 5px;
  height: 10px;
  /*border: solid white;*/
   border: solid black;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}

</style>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="BodyPlaceHolder" runat="server">
  
<div style="margin-left:50px;">


<h1>Custom Checkboxes</h1>
<label class="container">One
  <input type="checkbox" checked="checked">
  <span class="checkmark"></span>
</label>
<label class="container">Two
  <input type="checkbox">
  <span class="checkmark"></span>
</label>
<label class="container">Three
  <input type="checkbox">
  <span class="checkmark"></span>
</label>
<label class="container">Four
  <input type="checkbox" id="chk1">
  <span class="checkmark"></span>
</label>

  <asp:Table runat="server">
    <asp:TableRow>

      <asp:TableCell>
        <label class="container">
          <asp:CheckBox runat="server" id="chkRegionSSA" Checked="true"  Text="111"  />   
          <span class="checkmark"></span>
        </label>

      </asp:TableCell>
      <asp:TableCell>
        <label class="container">
          <asp:CheckBox runat="server" id="CheckBox1" Checked="true"  Text="2222"  />   
          <span class="checkmark"></span>
        </label>

      </asp:TableCell>
    </asp:TableRow>
  </asp:Table>

</div> 

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="FooterPlaceHolder" runat="server">
</asp:Content>
