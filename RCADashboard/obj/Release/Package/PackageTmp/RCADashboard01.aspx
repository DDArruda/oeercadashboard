﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="RCADashboard01.aspx.cs" Inherits="RCADashboard.RCADashboard01" %>

<asp:Content ID="Content3" ContentPlaceHolderID="PageTitlePlaceHolder" runat="server">
    RCA DashBoard
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">


  <script src="scripts/jquery-3.4.1.min.js"></script>
  <script src="scripts/bootstrap.min.js"></script>

  <link rel="stylesheet" type="text/css" href="Content/bootstrap.min.css" />

  <link rel="stylesheet" type="text/css" href="Content/jquery.datetimepicker.css" />

  <script src="scripts/jquery.datetimepicker.js"></script>

<script>



    function setStartDate()
    {
      document.getElementById("BodyPlaceHolder_hdnStartDate").value = document.getElementById("inpStartDate").value;
    }

    function setEndDate() {
      document.getElementById("BodyPlaceHolder_hdnEndDate").value = document.getElementById("inpEndDate").value;
    }
    
    function SelectAllSites(chk)
    {

      document.getElementById("BodyPlaceHolder_chkSiteNGX").checked = chk.checked;
      document.getElementById("BodyPlaceHolder_chkSiteSAI").checked = chk.checked;
      document.getElementById("BodyPlaceHolder_chkSiteSTA").checked = chk.checked;
      document.getElementById("BodyPlaceHolder_chkSiteTUG").checked = chk.checked;
    }


    function SelectAllPROC(chk)
    {
      var ele;
      try {
            for (i = 0; i < 25; i++) {
              ele = document.getElementById("BodyPlaceHolder_lstPrcGrp_" + i).value;
              if (ele  != null)
                document.getElementById("BodyPlaceHolder_lstPrcGrp_" + i).checked = chk.checked;
            }

        } // try
      catch (err)
      {
        ele = null;
      }
    } //


    function SelectAllRCAType(chk) {
      document.getElementById("BodyPlaceHolder_chkRCAOpen").checked = chk.checked;
      document.getElementById("BodyPlaceHolder_chkRCAClosed").checked = chk.checked;
      document.getElementById("BodyPlaceHolder_chkRCAOlder60").checked = chk.checked;
    } //




</script>


<style>

</style>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="BodyPlaceHolder" runat="server">

  <div style="margin:auto;width:99%;border:1px solid #006666;background-color:red;text-align:center;color:white;font-size:x-large;font-weight:bold;">
     <%--Sappi SA Root Cause Analysis (RCA) Dashboard --%>
      RCA Regional Manufacturing Database
      <span id="todaydate" style="float:right;margin-right:5px;font-size:medium;font-weight:normal;"></span>
  </div>

  <div style="margin:auto;width:70%;border:none;text-align:center;color:red;font-size:medium;">
     <asp:Label ID="lblMsg" runat="server" Text="" Font-Size="Larger"></asp:Label>

  </div>

  <div style="overflow-x:scroll;">


  <asp:Table id="tbl" runat="server" CssClass="centerTable">

   <asp:TableRow BackColor="#cce4f7">    

      <asp:TableCell ColumnSpan="2"  CssClass="tableBorder tablePadding" style="white-space:nowrap; vertical-align:top;">

      <b>Region:</b>
      <asp:CheckBox runat="server" id="chkRegionSSA" Checked="true"  Text="SSA" style="padding-left:10px;padding-right:40px;" />   

      <b>Site: </b> 
      <asp:CheckBox runat="server" id="chkSelAllSites" Text="Select/Deselect All"  style="padding-left:10px;padding-right:10px;" 
                         onclick="SelectAllSites(this);"/>

      <asp:CheckBox runat="server" id="chkSiteNGX" Text="NGX" ValidationGroup="sites" style="padding-left:10px;padding-right:10px;" />
      <asp:CheckBox runat="server" id="chkSiteSAI" Text="SAI" ValidationGroup="sites" style="padding-left:10px;padding-right:10px;" />
      <asp:CheckBox runat="server" id="chkSiteSTA" Text="STA" ValidationGroup="sites" style="padding-left:10px;padding-right:10px;" />
      <asp:CheckBox runat="server" id="chkSiteTUG" Text="TUG" ValidationGroup="sites" style="padding-left:10px;padding-right:10px;" />

      <asp:CheckBox runat="server" id="chkUnlimited" TextAlign="Left" Text="<b>Report Period: </b> Unlimited Time" 
                  style="padding-left:50px;padding-right:30px;" />

      <b>Start Date</b>
      <asp:hiddenfield id="hdnStartDate" runat="server"  value="" />
      <input id="inpStartDate" type="text" style="background-color:lightyellow" value="<%Response.Write(hdnStartDate.Value);%>" onchange="setStartDate()" />

      <b>End Date</b>
      <asp:hiddenfield id="hdnEndDate" runat="server"  value="" />
      <input id="inpEndDate" type="text" style="background-color:lightyellow" value="<%Response.Write(hdnEndDate.Value);%> " onchange="setEndDate()" />

       
     </asp:TableCell>


    </asp:TableRow>

    <asp:TableRow  CssClass="tableBorder"  BackColor="#cce4f7">
      <asp:TableCell Width="15%">
        <b>Process Area Groups:</b> <br />
        <asp:CheckBox runat="server" id="chkSelAllProc" Text="Select/Deselect All " TextAlign="Left" style="padding-left:10px;padding-right:10px;" 
              onclick="SelectAllPROC(this);"/>

      </asp:TableCell>
      <asp:TableCell  Width="85%">
          <asp:CheckBoxList runat="server" id="lstPrcGrp"  TextAlign="Right" RepeatColumns="7" RepeatDirection="Horizontal" 
                   RepeatLayout="Table" CellPadding="2" >
          </asp:CheckBoxList>
          <asp:Table runat="server" ID="tblCheck">

          </asp:Table>
      </asp:TableCell>
    </asp:TableRow>


    <asp:TableRow  CssClass="tableBorder"  BackColor="#cce4f7">
      <asp:TableCell Width="15%">
        <b>RCA Type:</b><br />
         <asp:CheckBox runat="server" id="chkRCAType" Text="Select/Deselect All " TextAlign="Left" style="padding-left:10px;padding-right:10px;" 
                onclick="SelectAllRCAType(this);"/>

      </asp:TableCell>
      <asp:TableCell Width="85%">
        <asp:CheckBox runat="server" id="chkRCAOpen" Text="Open" Checked="true" ValidationGroup="rcaType" style="padding-left:10px;padding-right:10px;" />
        <asp:CheckBox runat="server" id="chkRCAClosed" Text="Closed" ValidationGroup="rcaType" style="padding-left:10px;padding-right:10px;" />
        <asp:CheckBox runat="server" id="chkRCAOlder60" Text="Older than 60 days" Checked="false" ValidationGroup="rcaType" style="padding-left:10px;padding-right:10px;" />

        <asp:Button runat="server" id="cmdRunReport" Text="Run Report" OnClick="cmdRunReport_Click" 
              style="margin-left:40px;margin-right:10px;" CssClass="btn btn-info" >
        </asp:Button>
        
        <span id="runMsg" style="color:red"></span>

      </asp:TableCell>
    </asp:TableRow>


    <%-- --------------------TOTAL RCA STATS  -------------------------- --%>
    <asp:TableRow  BackColor="#c0e2be">         <%--greenish--%>
      <asp:TableCell ColumnSpan="2" HorizontalAlign="Center"  CssClass="tableBorder tablePadding">
           <asp:Label ID="Label1" runat="server" Text="RCA Stats Totals" Font-Bold="true" Font-Size="Medium"></asp:Label>
      </asp:TableCell>
    </asp:TableRow>


    <asp:TableRow >
      <asp:TableCell ColumnSpan="2">

        <asp:Table runat="server"  width="100%"  BackColor="#c0e2be">


          <asp:TableRow CssClass="tableBorder" >
            <asp:TableCell Width="5%"  HorizontalAlign="Center" CssClass="tableBorder tablePadding">
               <asp:Label ID="Label2" runat="server" Text="Region" Font-Bold="true" Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell Width="5%"  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="Label3" runat="server" Text="Site" Font-Bold="true" Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell Width="7%"  HorizontalAlign="Center"  CssClass="tableBorder">
               <asp:Label ID="Label10" runat="server" Text="System RCA Triggers (To Be Raised)"  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell Width="8%"  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="Label4" runat="server" Text="RCA's Initiated"  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell Width="8%"  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="Label11" runat="server" Text="RCA's In Progress"  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell Width="8%"  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="Label5" runat="server" Text="RCA's Not Required"  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell Width="8%"  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="Label12" runat="server" Text="RCA's Expected"  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell Width="8%"  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="Label14" runat="server" Text="RCA's Closed"  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell Width="8%"  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="Label15" runat="server" Text="Action Items" Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell Width="8%"  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="Label16" runat="server" Text="Action Items Closed"  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell Width="8%"  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="Label7" runat="server" Text="Duration RCA Open (Average Days)"  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
          </asp:TableRow>

          <asp:TableRow >
            <asp:TableCell>
              &nbsp;
            </asp:TableCell>
            <asp:TableCell>
               <asp:Label ID="Label9" runat="server" Text="NGX"  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCASysTriggersNGX" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAInitiatedNGX" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAInProgressNGX" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCANotRequiredNGX" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAExpectedNGX" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAClosedNGX" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblTotActionItemsNGX" runat="server" Text="" Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblTotActionItemsClosedNGX" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblDurationOpenNGX" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
          </asp:TableRow>


          <asp:TableRow >
            <asp:TableCell>
               <asp:Label ID="Label28" runat="server" Text="SSA"  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell >
               <asp:Label ID="Label8" runat="server" Text="SAI" Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCASysTriggersSAI" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAInitiatedSAI" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAInProgressSAI" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCANotRequiredSAI" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAExpectedSAI" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAClosedSAI" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblTotActionItemsSAI" runat="server" Text="" Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblTotActionItemsClosedSAI" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblDurationOpenSAI" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
          </asp:TableRow>


          <asp:TableRow>
            <asp:TableCell >
               &nbsp;
            </asp:TableCell>
            <asp:TableCell >
               <asp:Label ID="Label18" runat="server" Text="STA"  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCASysTriggersSTA" runat="server" Text="" Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAInitiatedSTA" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAInProgressSTA" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCANotRequiredSTA" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAExpectedSTA" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAClosedSTA" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblTotActionItemsSTA" runat="server" Text="" Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblTotActionItemsClosedSTA" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblDurationOpenSTA" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
          </asp:TableRow>


          <asp:TableRow>
            <asp:TableCell>
               &nbsp;
            </asp:TableCell>
            <asp:TableCell >
               <asp:Label ID="Label23" runat="server" Text="TUG"  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCASysTriggersTUG" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAInitiatedTUG" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAInProgressTUG" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCANotRequiredTUG" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAExpectedTUG" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAClosedTUG" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblTotActionItemsTUG" runat="server" Text="" Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblTotActionItemsClosedTUG" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblDurationOpenTUG" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
          </asp:TableRow>

         </asp:Table>
         

      </asp:TableCell>
    </asp:TableRow>


        <%--RCA Details and Link to RCA Report--%>


    <asp:TableRow  BackColor="RoyalBlue" ForeColor="White">
      <asp:TableCell ColumnSpan="2"  HorizontalAlign="Center" CssClass="tableBorder tablePadding">
           <asp:Label ID="Label29" runat="server" Text="RCA Details" Font-Bold="true" Font-Size="Medium"></asp:Label>
      </asp:TableCell>
    </asp:TableRow>




    <asp:TableRow  BackColor="RoyalBlue" ForeColor="WhiteSmoke">

      <asp:TableCell ColumnSpan="2">

        <asp:Table id="tblItems" runat="server"  width="100%">

          <asp:TableHeaderRow CssClass="tableBorder" BackColor="RoyalBlue"  HorizontalAlign="Center">
            <asp:TableHeaderCell Width="5%" CssClass="tableBorder">
               <asp:Label ID="Label31" runat="server" Text="Region" Font-Bold="true" Font-Size="Medium"></asp:Label>
            </asp:TableHeaderCell>
            <asp:TableHeaderCell Width="5%" CssClass="tableBorder" >
               <asp:Label ID="Label32" runat="server" Text="Site" Font-Bold="true" Font-Size="Medium"></asp:Label>
            </asp:TableHeaderCell>
            <asp:TableHeaderCell Width="10%" CssClass="tableBorder">
               <asp:Label ID="Label33" runat="server" Text="Process Area Group" Font-Bold="true" Font-Size="Medium"></asp:Label>
            </asp:TableHeaderCell>
            <asp:TableHeaderCell Width="15%" CssClass="tableBorder">
               <%--<asp:Label ID="Label6" runat="server" Text="Process Area" Font-Bold="true" Font-Size="Medium"></asp:Label>--%>
              <asp:DropDownList ID="cboProcessArea" runat="server" OnSelectedIndexChanged="cboProcessArea_SelectedIndexChanged" 
                  AutoPostBack="true" Font-Bold="true" Font-Size="Medium" BackColor="RoyalBlue" ForeColor="White">
              </asp:DropDownList>
            </asp:TableHeaderCell>
            <asp:TableHeaderCell Width="5%" CssClass="tableBorder" >
               <asp:Label ID="Label30" runat="server" Text="RCA Code" Font-Bold="true" Font-Size="Medium"></asp:Label>
            </asp:TableHeaderCell>
            <asp:TableHeaderCell Width="5%" CssClass="tableBorder" >
<%--               <asp:Label ID="Label34" runat="server" Text="RCA Status" Font-Bold="true" Font-Size="Medium"></asp:Label>--%>
              <asp:DropDownList ID="cboRCAStatus" runat="server" OnSelectedIndexChanged="cboRCAStatus_SelectedIndexChanged" 
                  AutoPostBack="true" Font-Bold="true" Font-Size="Medium" BackColor="RoyalBlue" ForeColor="White">
              </asp:DropDownList>

            </asp:TableHeaderCell>
            <asp:TableHeaderCell Width="5%" CssClass="tableBorder" >
               <asp:Label ID="Label35" runat="server" Text="Total # Action Items" Font-Bold="true" Font-Size="Medium"></asp:Label>
            </asp:TableHeaderCell>
            <asp:TableHeaderCell Width="5%" CssClass="tableBorder" >
               <asp:Label ID="Label57" runat="server" Text="# Action Items Open" Font-Bold="true" Font-Size="Medium"></asp:Label>
            </asp:TableHeaderCell>
            <asp:TableHeaderCell Width="5%" CssClass="tableBorder" >
               <asp:Label ID="Label58" runat="server" Text="Duration Open days" Font-Bold="true" Font-Size="Medium"></asp:Label>
            </asp:TableHeaderCell>
            <asp:TableHeaderCell Width="15%" CssClass="tableBorder">
               <asp:Label ID="Label59" runat="server" Text="RCA Description" Font-Bold="true" Font-Size="Medium"></asp:Label>
            </asp:TableHeaderCell>
            <asp:TableHeaderCell Width="8%" CssClass="tableBorder">
               <asp:Label ID="Label60" runat="server" Text="Responsible Manager" Font-Bold="true" Font-Size="Medium"></asp:Label>
            </asp:TableHeaderCell>
          </asp:TableHeaderRow>


         </asp:Table>



       </asp:TableCell>
    </asp:TableRow>


  </asp:Table>

  <div style="width:70%;border:none;text-align:center;color:teal;font-size:medium;">
    <asp:Label ID="lblNumRows" runat="server" Text=""></asp:Label>
  </div>


  </div>
  

  
<script>
//--https://www.jqueryscript.net/time-clock/Clean-jQuery-Date-Time-Picker-Plugin-datetimepicker.html

  $( function() {
    $("#inpStartDate").datetimepicker({
      //inline: true     always visible
      format:'Y-m-d',
      closeOnDateSelect: true,
      datepicker:true,
      timepicker: false,
      initTime:false,
      //closeOnTimeSelect:true,
    })
  });

   $( function() {
     $("#inpEndDate").datetimepicker({
       //inline: true     always visible
       format: 'Y-m-d',
       closeOnDateSelect: true,
       datepicker: true,
       timepicker: false,
       initTime: false,
       //closeOnTimeSelect:true,
     });
  });


   function pad(number, length)
   {

     var str = '' + number;
     while (str.length < length) {
       str = '0' + str;
     }

     return str;
   }


   showTime();   //initial load
   var i = setInterval(showTime, 60000);


   function showTime()
   {
     console.log("showTime");
     var d = new Date();
     document.getElementById("todaydate").innerHTML = d.getFullYear() + '-' + pad(d.getMonth(), 2) + '-' + pad(d.getDate(), 2)
                                + ' ' + pad(d.getHours(),2) + ':' + pad(d.getMinutes(),2);
   }


   $(document).ready(function ()
   {
     $("#runMsg").hide();
     $("#runMsg").html("");

     $("#BodyPlaceHolder_cmdRunReport").on("click", function ()
     {
       //console.log("BodyPlaceHolder_cmdRunReport clicked");
       $("#runMsg").html("Fetching data....");
       $("#runMsg").show();

     });

     $("#BodyPlaceHolder_cboProcessArea").on("change", function () {
       $("#runMsg").html("Fetching data....");
       $("#runMsg").show();

     });


     $("#BodyPlaceHolder_cboRCAStatus").on("change", function () {
       $("#runMsg").html("Fetching data....");
       $("#runMsg").show();
     });

   });   





  //function myFunction()
  //{
  //var ele = document.querySelectorAll('[id^=textbox]');

  //  var x = document.getElementById("tbl").querySelectorAll(".classname");
  //  var i;
  //  for (i = 0; i < x.length; i++) {
  //    x[i].style.backgroundColor = "red";
  //  }
  //}

</script>




<%--
  <div class="container jumbotron">

  jumbotron baby
</div>

--%>

</asp:Content>
