﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Test4.aspx.cs" Inherits="RCADashboard.Test4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageTitlePlaceHolder" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="server">

   <style>


.tableFixHead          { overflow-y: auto; height: 500px; }
.tableFixHead thead th { position: sticky; top: 0; color:blue; }


/* Just common table stuff. Really. */
table  { border-collapse: collapse; width: 100%; }
th, td { padding: 6px 0px; }
th     { background:#eee; }


   </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="BodyPlaceHolder" runat="server">



 <div class="tableFixHead">
            <table>
              <thead>
                <tr>
                <th>TH 1</th>
                <th>TH 2</th>
                </tr>
              </thead>
              <tbody>
                <tr><td>A1</td><td>A2</td></tr>
                <tr><td>B1</td><td>B2</td></tr>
                <tr><td>C1</td><td>C2</td></tr>
                <tr><td>D1</td><td>D2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>A1</td><td>A2</td></tr>
                <tr><td>B1</td><td>B2</td></tr>
                <tr><td>C1</td><td>C2</td></tr>
                <tr><td>D1</td><td>D2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>A1</td><td>A2</td></tr>
                <tr><td>B1</td><td>B2</td></tr>
                <tr><td>C1</td><td>C2</td></tr>
                <tr><td>D1</td><td>D2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                <tr><td>E1</td><td>E2</td></tr>
                
                <tr><td>the end</td><td>E2</td></tr>
              </tbody>
            </table>
          </div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="FooterPlaceHolder" runat="server">

</asp:Content>
