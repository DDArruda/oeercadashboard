﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="RCADashboard02.aspx.cs" Inherits="RCADashboard.RCADashboard02" %>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitlePlaceHolder" runat="server">
    RCA DashBoard
</asp:Content>

<%--LAST UPDATE 2020-05-20--%>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">


  <script src="scripts/jquery-3.4.1.min.js"></script>
  <script src="scripts/bootstrap.min.js"></script>

  <link rel="stylesheet" type="text/css" href="Content/bootstrap.min.css" />

  <link rel="stylesheet" type="text/css" href="Content/jquery.datetimepicker.css" />

  <script src="scripts/jquery.datetimepicker.js"></script>

<script>



    function setStartDate()
    {
      document.getElementById("BodyPlaceHolder_hdnStartDate").value = document.getElementById("inpStartDate").value;
    }

    function setEndDate() {
      document.getElementById("BodyPlaceHolder_hdnEndDate").value = document.getElementById("inpEndDate").value;
    }
    
    function SelectAllSites(chk)
    {

      document.getElementById("chkSiteNGX").checked = chk.checked;
      document.getElementById("chkSiteSAI").checked = chk.checked;
      document.getElementById("chkSiteSTA").checked = chk.checked;
      document.getElementById("chkSiteTUG").checked = chk.checked;
    }


    function SelectAllPROC(chk) {
      var ele;
      try {
        document.getElementById("chkPrcGrp1").checked = chk.checked;
        document.getElementById("chkPrcGrp2").checked = chk.checked;
        document.getElementById("chkPrcGrp3").checked = chk.checked;
        document.getElementById("chkPrcGrp4").checked = chk.checked;
        document.getElementById("chkPrcGrp5").checked = chk.checked;
        document.getElementById("chkPrcGrp6").checked = chk.checked;
        document.getElementById("chkPrcGrp7").checked = chk.checked;
        document.getElementById("chkPrcGrp8").checked = chk.checked;
        document.getElementById("chkPrcGrp9").checked = chk.checked;
        document.getElementById("chkPrcGrp10").checked = chk.checked;
        document.getElementById("chkPrcGrp11").checked = chk.checked;
        document.getElementById("chkPrcGrp12").checked = chk.checked;
        document.getElementById("chkPrcGrp13").checked = chk.checked;
        document.getElementById("chkPrcGrp14").checked = chk.checked;
        document.getElementById("chkPrcGrp15").checked = chk.checked;
        document.getElementById("chkPrcGrp16").checked = chk.checked;
        document.getElementById("chkPrcGrp17").checked = chk.checked;
        document.getElementById("chkPrcGrp18").checked = chk.checked;
        document.getElementById("chkPrcGrp19").checked = chk.checked;

      } // try
      catch (err) {
        //console.log("ele = catch = ");
        ele = null;
      }
    } //


    function SelectAllRCAType(chk) {
      document.getElementById("chkRCAOpen").checked = chk.checked;
      document.getElementById("chkRCAClosed").checked = chk.checked;
      document.getElementById("chkRCAOlder60").checked = chk.checked;
    } //


    function LaunchRCAFinder()
    {
      window.open("http://ngxvmii03:50000/XMII/CM/Ngx_OEE/Reporting/OEERCAReport/OEERCAFinder.irpt");
    }

</script>


<style>
    .custom-checkbox .custom-control-input:checked ~ .custom-control-label::before 
 {
      background-color:lawngreen!important;
 }

  .custom-checkbox .custom-control-input:not(checked) ~ .custom-control-label::before 
 {
      background-color:darkgoldenrod!important;
 }

</style>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="BodyPlaceHolder" runat="server">

  <div style="margin:auto;width:99%;border:1px solid #006666;background-color:red;text-align:center;color:white;font-size:x-large;font-weight:bold;">
      RCA Regional Manufacturing Database
      <span id="todaydate" style="float:right;margin-right:5px;font-size:medium;font-weight:normal;"></span>
  </div>

  <div style="margin:auto;width:70%;border:none;text-align:center;color:red;font-size:medium;">
     <asp:Label ID="lblMsg" runat="server" Text="" Font-Size="Larger"></asp:Label>

  </div>

  <div style="overflow-x:scroll;">

  <asp:Table id="tbl" runat="server" CssClass="centerTable">

   <asp:TableRow BackColor="#cce4f7">    

      <asp:TableCell style="white-space:nowrap;vertical-align:top;">
          <b>Region:</b>      
      </asp:TableCell>

      <asp:TableCell   style="white-space:nowrap;vertical-align:top;">
          <div class="custom-control custom-checkbox custom-control-inline">
              <input  type="checkbox" id="chkRegionSSA" name="chkRegionSSA" value="SSA"  class="custom-control-input" <%Response.Write(hdnRegionSSA.Value);%> />
              <label class="custom-control-label" for="chkRegionSSA">SSA</label>
          </div>
      </asp:TableCell>

      <asp:TableCell style="white-space:nowrap;vertical-align:top;">
          <b>Site: </b> 
      </asp:TableCell>

      <asp:TableCell style="white-space:nowrap;vertical-align:top;">
          <div class="custom-control custom-checkbox custom-control-inline">
              <input  type="checkbox" id="chkSelAllSites" name="chkSelAllSites"  class="custom-control-input"
                 <%Response.Write(hdnSelAllSites.Value);%> onclick="SelectAllSites(this);"/>
              <label class="custom-control-label" for="chkSelAllSites">Select/Deselect All</label>
          </div>
      </asp:TableCell>

      <asp:TableCell style="white-space:nowrap;vertical-align:top;">
          <div class="custom-control custom-checkbox custom-control-inline">
              <input  type="checkbox" id="chkSiteNGX" name="chkSiteNGX" value="NGX" class="custom-control-input"
                 <%Response.Write(hdnSiteNGX.Value);%>/>
              <label class="custom-control-label" for="chkSiteNGX">NGX</label>
          </div>
      </asp:TableCell>

      <asp:TableCell style="white-space:nowrap;vertical-align:top;">
          <div class="custom-control custom-checkbox custom-control-inline">
              <input  type="checkbox" id="chkSiteSAI" name="chkSiteSAI" value="SAI" class="custom-control-input"
                 <%Response.Write(hdnSiteSAI.Value);%>/>
              <label class="custom-control-label" for="chkSiteSAI">SAI</label>
          </div>
      </asp:TableCell>

      <asp:TableCell style="white-space:nowrap;vertical-align:top;">
          <div class="custom-control custom-checkbox custom-control-inline">
              <input  type="checkbox" id="chkSiteSTA" name="chkSiteSTA" value="STA" class="custom-control-input"
                 <%Response.Write(hdnSiteSTA.Value);%>/>
              <label class="custom-control-label" for="chkSiteSTA">STA</label>
          </div>

      </asp:TableCell>

      <asp:TableCell style="white-space:nowrap;vertical-align:top;">
          <div class="custom-control custom-checkbox custom-control-inline">
              <input  type="checkbox" id="chkSiteTUG" name="chkSiteTUG" value="TUG" class="custom-control-input"
                 <%Response.Write(hdnSiteTUG.Value);%>/>
              <label class="custom-control-label" for="chkSiteTUG">TUG</label>
          </div>
      </asp:TableCell>

      <asp:TableCell style="white-space:nowrap;vertical-align:top;">
        <b>Report Period:</b>
      </asp:TableCell>

      <asp:TableCell style="white-space:nowrap;vertical-align:top;">
          <div class="custom-control custom-checkbox custom-control-inline">
              <input  type="checkbox" id="chkUnlimited" name="chkUnlimited" value="Unlimited" class="custom-control-input"
                 <%Response.Write(hdnUnlimited.Value);%>/>
              <label class="custom-control-label" for="chkUnlimited"> Unlimited Time</label>
          </div>

      </asp:TableCell>

      <asp:TableCell style="white-space:nowrap;vertical-align:top;">
              <b>Start Date</b>
      </asp:TableCell>

      <asp:TableCell style="white-space:nowrap;vertical-align:top;">
          <asp:hiddenfield id="hdnStartDate" runat="server"  value="" />
          <input id="inpStartDate" type="text" style="background-color:lightyellow" value="<%Response.Write(hdnStartDate.Value);%>" onchange="setStartDate()" />
      </asp:TableCell>

      <asp:TableCell style="white-space:nowrap;vertical-align:top;">
          <b>End Date</b>
      </asp:TableCell>

      <asp:TableCell style="white-space:nowrap;vertical-align:top;">
          <asp:hiddenfield id="hdnEndDate" runat="server"  value="" />
          <input id="inpEndDate" type="text" style="background-color:lightyellow" value="<%Response.Write(hdnEndDate.Value);%> " onchange="setEndDate()" />
      </asp:TableCell>
    </asp:TableRow>


    <asp:TableRow  CssClass="tableBorder"  BackColor="#cce4f7">
      <asp:TableCell  ColumnSpan="3">
        <b>Process Area Groups:</b> <br />
        <div class="custom-control custom-checkbox custom-control-inline ml-4">
           <input  type="checkbox" id="chkSelAllProc" name="chkSelAllProc" value="SelAllProc" class="custom-control-input"
                <%Response.Write(hdnSelAllProc.Value);%> onclick="SelectAllPROC(this);"/>
            <label class="custom-control-label" for="chkSelAllProc">Select/Deselect All</label>
        </div>

      </asp:TableCell>


      <asp:TableCell ColumnSpan="11">
          <asp:Table runat="server" ID="tblLstProc">
            <asp:TableRow>
              <asp:TableCell>
                  <div class="custom-control custom-checkbox custom-control-inline">
                     <input  type="checkbox" id="chkPrcGrp1" name="chkPrcGrp1" value="<%Response.Write(hdnLstProc1.Value);%>" 
                       class="custom-control-input"  <%Response.Write(hdnLstProcChk1.Value);%> >
                      <label class="custom-control-label" for="chkPrcGrp1"><%Response.Write(hdnLstProcTxt1.Value);%></label>
                  </div>
              </asp:TableCell>

              <asp:TableCell>
                  <div class="custom-control custom-checkbox custom-control-inline">
                     <input  type="checkbox" id="chkPrcGrp2" name="chkPrcGrp2" value="<%Response.Write(hdnLstProc2.Value);%>" 
                       class="custom-control-input" <%Response.Write(hdnLstProcChk2.Value);%> >
                      <label class="custom-control-label" for="chkPrcGrp2"><%Response.Write(hdnLstProcTxt2.Value);%></label>
                  </div>
              </asp:TableCell>


              <asp:TableCell>
                  <div class="custom-control custom-checkbox custom-control-inline">
                     <input  type="checkbox" id="chkPrcGrp3" name="chkPrcGrp1" value="<%Response.Write(hdnLstProc3.Value);%>" 
                       class="custom-control-input" <%Response.Write(hdnLstProcChk3.Value);%> >
                      <label class="custom-control-label" for="chkPrcGrp3"><%Response.Write(hdnLstProcTxt3.Value);%></label>
                  </div>
              </asp:TableCell>

              <asp:TableCell>
                  <div class="custom-control custom-checkbox custom-control-inline">
                     <input  type="checkbox" id="chkPrcGrp4" name="chkPrcGrp4" value="<%Response.Write(hdnLstProc4.Value);%>" 
                       class="custom-control-input"  <%Response.Write(hdnLstProcChk4.Value);%> >
                      <label class="custom-control-label" for="chkPrcGrp4"><%Response.Write(hdnLstProcTxt4.Value);%></label>
                  </div>
              </asp:TableCell>

              <asp:TableCell>
                  <div class="custom-control custom-checkbox custom-control-inline">
                     <input  type="checkbox" id="chkPrcGrp5" name="chkPrcGrp5" value="<%Response.Write(hdnLstProc5.Value);%>" 
                       class="custom-control-input"  <%Response.Write(hdnLstProcChk5.Value);%> >
                      <label class="custom-control-label" for="chkPrcGrp5"><%Response.Write(hdnLstProcTxt5.Value);%></label>
                  </div>
              </asp:TableCell>

              <asp:TableCell>
                  <div class="custom-control custom-checkbox custom-control-inline">
                     <input  type="checkbox" id="chkPrcGrp6" name="chkPrcGrp6" value="<%Response.Write(hdnLstProc6.Value);%>" 
                       class="custom-control-input"  <%Response.Write(hdnLstProcChk6.Value);%> >
                      <label class="custom-control-label" for="chkPrcGrp6"><%Response.Write(hdnLstProcTxt6.Value);%></label>
                  </div>
              </asp:TableCell>

              <asp:TableCell>
                  <div class="custom-control custom-checkbox custom-control-inline">
                     <input  type="checkbox" id="chkPrcGrp7" name="chkPrcGrp7" value="<%Response.Write(hdnLstProc7.Value);%>" 
                       class="custom-control-input"  <%Response.Write(hdnLstProcChk7.Value);%> >
                      <label class="custom-control-label" for="chkPrcGrp7"><%Response.Write(hdnLstProcTxt7.Value);%></label>
                  </div>
              </asp:TableCell>
            </asp:TableRow>

            <asp:TableRow>
              <asp:TableCell>
                  <div class="custom-control custom-checkbox custom-control-inline">
                     <input  type="checkbox" id="chkPrcGrp8" name="chkPrcGrp8" value="<%Response.Write(hdnLstProc8.Value);%>" 
                       class="custom-control-input"  <%Response.Write(hdnLstProcChk8.Value);%> >
                      <label class="custom-control-label" for="chkPrcGrp8"><%Response.Write(hdnLstProcTxt8.Value);%></label>
                  </div>
              </asp:TableCell>

              <asp:TableCell>
                  <div class="custom-control custom-checkbox custom-control-inline">
                     <input  type="checkbox" id="chkPrcGrp9" name="chkPrcGrp9" value="<%Response.Write(hdnLstProc9.Value);%>" 
                       class="custom-control-input"  <%Response.Write(hdnLstProcChk9.Value);%> >
                      <label class="custom-control-label" for="chkPrcGrp9"><%Response.Write(hdnLstProcTxt9.Value);%></label>
                  </div>
              </asp:TableCell>

              <asp:TableCell>
                  <div class="custom-control custom-checkbox custom-control-inline">
                     <input  type="checkbox" id="chkPrcGrp10" name="chkPrcGrp10" value="<%Response.Write(hdnLstProc10.Value);%>" 
                       class="custom-control-input"  <%Response.Write(hdnLstProcChk10.Value);%> >
                      <label class="custom-control-label" for="chkPrcGrp10"><%Response.Write(hdnLstProcTxt10.Value);%></label>
                  </div>
              </asp:TableCell>

              <asp:TableCell>
                  <div class="custom-control custom-checkbox custom-control-inline">
                     <input  type="checkbox" id="chkPrcGrp11" name="chkPrcGrp11" value="<%Response.Write(hdnLstProc11.Value);%>" 
                       class="custom-control-input"  <%Response.Write(hdnLstProcChk11.Value);%> >
                      <label class="custom-control-label" for="chkPrcGrp11"><%Response.Write(hdnLstProcTxt11.Value);%></label>
                  </div>
              </asp:TableCell>

              <asp:TableCell>
                  <div class="custom-control custom-checkbox custom-control-inline">
                     <input  type="checkbox" id="chkPrcGrp12" name="chkPrcGrp12" value="<%Response.Write(hdnLstProc12.Value);%>" 
                       class="custom-control-input"  <%Response.Write(hdnLstProcChk12.Value);%> >
                      <label class="custom-control-label" for="chkPrcGrp12"><%Response.Write(hdnLstProcTxt12.Value);%></label>
                  </div>
              </asp:TableCell>

              <asp:TableCell>
                  <div class="custom-control custom-checkbox custom-control-inline">
                     <input  type="checkbox" id="chkPrcGrp13" name="chkPrcGrp13" value="<%Response.Write(hdnLstProc13.Value);%>" 
                       class="custom-control-input"  <%Response.Write(hdnLstProcChk13.Value);%> >
                      <label class="custom-control-label" for="chkPrcGrp13"><%Response.Write(hdnLstProcTxt13.Value);%></label>
                  </div>
              </asp:TableCell>

              <asp:TableCell>
                  <div class="custom-control custom-checkbox custom-control-inline">
                     <input  type="checkbox" id="chkPrcGrp14" name="chkPrcGrp14" value="<%Response.Write(hdnLstProc14.Value);%>" 
                       class="custom-control-input"  <%Response.Write(hdnLstProcChk14.Value);%> >
                      <label class="custom-control-label" for="chkPrcGrp14"><%Response.Write(hdnLstProcTxt14.Value);%></label>
                  </div>
              </asp:TableCell>

            </asp:TableRow>

            <asp:TableRow>
              <asp:TableCell>
                  <div class="custom-control custom-checkbox custom-control-inline">
                     <input  type="checkbox" id="chkPrcGrp15" name="chkPrcGrp15" value="<%Response.Write(hdnLstProc15.Value);%>" 
                       class="custom-control-input"  <%Response.Write(hdnLstProcChk15.Value);%> >
                      <label class="custom-control-label" for="chkPrcGrp15"><%Response.Write(hdnLstProcTxt15.Value);%></label>
                  </div>
              </asp:TableCell>

              <asp:TableCell>
                  <div class="custom-control custom-checkbox custom-control-inline">
                     <input  type="checkbox" id="chkPrcGrp16" name="chkPrcGrp16" value="<%Response.Write(hdnLstProc16.Value);%>" 
                       class="custom-control-input"  <%Response.Write(hdnLstProcChk16.Value);%> >
                      <label class="custom-control-label" for="chkPrcGrp16"><%Response.Write(hdnLstProcTxt16.Value);%></label>
                  </div>
              </asp:TableCell>

              <asp:TableCell>
                  <div class="custom-control custom-checkbox custom-control-inline">
                     <input  type="checkbox" id="chkPrcGrp17" name="chkPrcGrp17" value="<%Response.Write(hdnLstProc17.Value);%>" 
                       class="custom-control-input"  <%Response.Write(hdnLstProcChk17.Value);%> >
                      <label class="custom-control-label" for="chkPrcGrp17"><%Response.Write(hdnLstProcTxt17.Value);%></label>
                  </div>
              </asp:TableCell>

              <asp:TableCell>
                  <div class="custom-control custom-checkbox custom-control-inline">
                     <input  type="checkbox" id="chkPrcGrp18" name="chkPrcGrp1" value="<%Response.Write(hdnLstProc18.Value);%>" 
                       class="custom-control-input"  <%Response.Write(hdnLstProcChk18.Value);%> >
                      <label class="custom-control-label" for="chkPrcGrp18"><%Response.Write(hdnLstProcTxt18.Value);%></label>
                  </div>
              </asp:TableCell>

              <asp:TableCell>
                  <div class="custom-control custom-checkbox custom-control-inline">
                     <input  type="checkbox" id="chkPrcGrp19" name="chkPrcGrp19" value="<%Response.Write(hdnLstProc19.Value);%>" 
                       class="custom-control-input"  <%Response.Write(hdnLstProcChk19.Value);%> >
                      <label class="custom-control-label" for="chkPrcGrp19"><%Response.Write(hdnLstProcTxt19.Value);%></label>
                  </div>
              </asp:TableCell>

              <asp:TableCell>
                  <div class="custom-control custom-checkbox custom-control-inline">
                     <input  type="checkbox" id="chkPrcGrp20" hidden name="chkPrcGrp20" value="<%Response.Write(hdnLstProc20.Value);%>" 
                       class="custom-control-input"   <%Response.Write(hdnLstProcChk20.Value);%> >
                      <label class="custom-control-label" hidden for="chkPrcGrp20"><%Response.Write(hdnLstProcTxt20.Value);%></label>
                  </div>
              </asp:TableCell>

              <asp:TableCell>
                  <div class="custom-control custom-checkbox custom-control-inline">
                     <input  type="checkbox" id="chkPrcGrp21" hidden name="chkPrcGrp21" value="<%Response.Write(hdnLstProc21.Value);%>" 
                       class="custom-control-input"  <%Response.Write(hdnLstProcChk21.Value);%> >
                      <label class="custom-control-label" hidden for="chkPrcGrp21"><%Response.Write(hdnLstProcTxt21.Value);%></label>
                  </div>
              </asp:TableCell>
            </asp:TableRow>
          </asp:Table>
      </asp:TableCell>
    </asp:TableRow>


    <asp:TableRow  CssClass="tableBorder"  BackColor="#cce4f7">
      <asp:TableCell ColumnSpan="3">
        <b>RCA Type:</b><br />
        <div class="custom-control custom-checkbox custom-control-inline ml-4">
           <input  type="checkbox" id="chkRCAType" name="chkRCAType" value="RCAType" class="custom-control-input"
                <%Response.Write(hdnRCAType.Value);%> onclick="SelectAllRCAType(this);"/>
            <label class="custom-control-label" for="chkRCAType">Select/Deselect All</label>
        </div>
      </asp:TableCell>

      <asp:TableCell ColumnSpan="11" >

        <asp:Table runat="server">
          <asp:TableRow>
            <asp:TableCell>
               <div class="custom-control custom-checkbox custom-control-inline">
                   <input  type="checkbox" id="chkRCAOpen" name="chkRCAOpen" value="RCAOpen" class="custom-control-input"
                        <%Response.Write(hdnRCAOpen.Value);%>/>
                    <label class="custom-control-label" for="chkRCAOpen">Open</label>
                </div>

            </asp:TableCell>
            <asp:TableCell>
               <div class="custom-control custom-checkbox custom-control-inline">
                   <input  type="checkbox" id="chkRCAClosed" name="chkRCAClosed" value="RCAClosed" class="custom-control-input"
                        <%Response.Write(hdnRCAClosed.Value);%>/>
                    <label class="custom-control-label" for="chkRCAClosed">Closed</label>
                </div>

            </asp:TableCell>
            <asp:TableCell>
               <div class="custom-control custom-checkbox custom-control-inline">
                   <input  type="checkbox" id="chkRCAOlder60" name="chkRCAOlder60" value="older60" class="custom-control-input"
                        <%Response.Write(hdnOlder60days.Value);%>/>
                    <label class="custom-control-label" for="chkRCAOlder60">Older than 60 days</label>
                </div>

            </asp:TableCell>
            <asp:TableCell>
              <asp:Button runat="server" id="cmdRunReport" Text="Run Report" OnClick="cmdRunReport_Click" 
                    style="margin-left:40px;margin-right:10px;" CssClass="btn btn-info" >
              </asp:Button>
            </asp:TableCell>
            <asp:TableCell Width="150px">
                <span id="runMsg" style="color:red;"></span>
            </asp:TableCell>
            <asp:TableCell>
              <asp:Button runat="server" id="cmdRCAFinder" Text="RCA Finder"  OnClientClick="LaunchRCAFinder()"
                    style="margin-left:40px;margin-right:10px;" CssClass="btn btn-info" >
              </asp:Button>
            </asp:TableCell>

          </asp:TableRow>
        </asp:Table>

      </asp:TableCell>

    </asp:TableRow>


    <%-- --------------------TOTAL RCA STATS  -------------------------- --%>
    <asp:TableRow  BackColor="#c0e2be">         <%--greenish--%>
      <asp:TableCell ColumnSpan="14" HorizontalAlign="Center"  CssClass="tableBorder tablePadding">
           <asp:Label ID="Label1" runat="server" Text="RCA Stats Totals" Font-Bold="true" Font-Size="Medium"></asp:Label>
      </asp:TableCell>
    </asp:TableRow>


    <asp:TableRow >

      <asp:TableCell ColumnSpan="14">

        <asp:Table runat="server"  width="100%"  BackColor="#c0e2be">


          <asp:TableRow CssClass="tableBorder" >
            <asp:TableCell Width="5%"  HorizontalAlign="Center" CssClass="tableBorder tablePadding">
               <asp:Label ID="Label2" runat="server" Text="Region" Font-Bold="true" Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell Width="5%"  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="Label3" runat="server" Text="Site" Font-Bold="true" Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell Width="7%"  HorizontalAlign="Center"  CssClass="tableBorder">
               <asp:Label ID="Label10" runat="server" Text="System RCA Triggers (To Be Raised)"  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell Width="8%"  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="Label4" runat="server" Text="RCA's Initiated"  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell Width="8%"  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="Label11" runat="server" Text="RCA's In Progress"  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell Width="8%"  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="Label5" runat="server" Text="RCA's Not Required"  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell Width="8%"  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="Label12" runat="server" Text="RCA's Expected"  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell Width="8%"  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="Label14" runat="server" Text="RCA's Closed"  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell Width="8%"  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="Label15" runat="server" Text="Action Items" Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell Width="8%"  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="Label16" runat="server" Text="Action Items Closed"  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell Width="8%"  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="Label7" runat="server" Text="Duration RCA Open (Average Days)"  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
          </asp:TableRow>

          <asp:TableRow >
            <asp:TableCell>
              &nbsp;
            </asp:TableCell>
            <asp:TableCell>
               <asp:Label ID="Label9" runat="server" Text="NGX"  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCASysTriggersNGX" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAInitiatedNGX" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAInProgressNGX" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCANotRequiredNGX" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAExpectedNGX" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAClosedNGX" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblTotActionItemsNGX" runat="server" Text="" Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblTotActionItemsClosedNGX" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblDurationOpenNGX" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
          </asp:TableRow>


          <asp:TableRow >
            <asp:TableCell>
               <asp:Label ID="Label28" runat="server" Text="SSA"  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell >
               <asp:Label ID="Label8" runat="server" Text="SAI" Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCASysTriggersSAI" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAInitiatedSAI" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAInProgressSAI" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCANotRequiredSAI" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell  HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAExpectedSAI" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAClosedSAI" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblTotActionItemsSAI" runat="server" Text="" Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblTotActionItemsClosedSAI" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblDurationOpenSAI" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
          </asp:TableRow>


          <asp:TableRow>
            <asp:TableCell >
               &nbsp;
            </asp:TableCell>
            <asp:TableCell >
               <asp:Label ID="Label18" runat="server" Text="STA"  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCASysTriggersSTA" runat="server" Text="" Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAInitiatedSTA" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAInProgressSTA" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCANotRequiredSTA" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAExpectedSTA" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAClosedSTA" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblTotActionItemsSTA" runat="server" Text="" Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblTotActionItemsClosedSTA" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblDurationOpenSTA" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
          </asp:TableRow>


          <asp:TableRow>
            <asp:TableCell>
               &nbsp;
            </asp:TableCell>
            <asp:TableCell >
               <asp:Label ID="Label23" runat="server" Text="TUG"  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCASysTriggersTUG" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAInitiatedTUG" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAInProgressTUG" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCANotRequiredTUG" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAExpectedTUG" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblRCAClosedTUG" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblTotActionItemsTUG" runat="server" Text="" Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblTotActionItemsClosedTUG" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" CssClass="tableBorder">
               <asp:Label ID="lblDurationOpenTUG" runat="server" Text=""  Font-Size="Medium"></asp:Label>
            </asp:TableCell>
          </asp:TableRow>

         </asp:Table>
         

      </asp:TableCell>
    </asp:TableRow>


        <%--RCA Details and Link to RCA Report--%>


    <asp:TableRow  BackColor="RoyalBlue" ForeColor="White">
      <asp:TableCell ColumnSpan="14"  HorizontalAlign="Center" CssClass="tableBorder tablePadding">
           <asp:Label ID="Label29" runat="server" Text="RCA Details" Font-Bold="true" Font-Size="Medium"></asp:Label>
      </asp:TableCell>
    </asp:TableRow>




    <asp:TableRow  BackColor="RoyalBlue" ForeColor="WhiteSmoke">

      <asp:TableCell ColumnSpan="14">

        <asp:Table id="tblItems" runat="server"  width="100%">

          <asp:TableHeaderRow CssClass="tableBorder" BackColor="RoyalBlue"  HorizontalAlign="Center">
            <asp:TableHeaderCell Width="5%" CssClass="tableBorder">
               <asp:Label ID="Label31" runat="server" Text="Region" Font-Bold="true" Font-Size="Medium"></asp:Label>
            </asp:TableHeaderCell>
            <asp:TableHeaderCell Width="5%" CssClass="tableBorder" >
               <asp:Label ID="Label32" runat="server" Text="Site" Font-Bold="true" Font-Size="Medium"></asp:Label>
            </asp:TableHeaderCell>
            <asp:TableHeaderCell Width="10%" CssClass="tableBorder">
               <asp:Label ID="Label33" runat="server" Text="Process Area Group" Font-Bold="true" Font-Size="Medium"></asp:Label>
            </asp:TableHeaderCell>
            <asp:TableHeaderCell Width="15%" CssClass="tableBorder">
              <asp:DropDownList ID="cboProcessArea" runat="server" OnSelectedIndexChanged="cboProcessArea_SelectedIndexChanged" 
                  AutoPostBack="true" Font-Bold="true" Font-Size="Medium" BackColor="RoyalBlue" ForeColor="White">
              </asp:DropDownList>
            </asp:TableHeaderCell>
            <asp:TableHeaderCell Width="5%" CssClass="tableBorder" >
               <asp:Label ID="Label30" runat="server" Text="RCA Code" Font-Bold="true" Font-Size="Medium"></asp:Label>
            </asp:TableHeaderCell>
            <asp:TableHeaderCell Width="5%" CssClass="tableBorder" >
              <asp:DropDownList ID="cboRCAStatus" runat="server" OnSelectedIndexChanged="cboRCAStatus_SelectedIndexChanged" 
                  AutoPostBack="true" Font-Bold="true" Font-Size="Medium" BackColor="RoyalBlue" ForeColor="White">
              </asp:DropDownList>

            </asp:TableHeaderCell>
            <asp:TableHeaderCell Width="5%" CssClass="tableBorder" >
               <asp:Label ID="Label35" runat="server" Text="Total # Action Items" Font-Bold="true" Font-Size="Medium"></asp:Label>
            </asp:TableHeaderCell>
            <asp:TableHeaderCell Width="5%" CssClass="tableBorder" >
               <asp:Label ID="Label57" runat="server" Text="# Action Items Open" Font-Bold="true" Font-Size="Medium"></asp:Label>
            </asp:TableHeaderCell>
            <asp:TableHeaderCell Width="5%" CssClass="tableBorder" >
               <asp:Label ID="Label58" runat="server" Text="Duration Open days" Font-Bold="true" Font-Size="Medium"></asp:Label>
            </asp:TableHeaderCell>
            <asp:TableHeaderCell Width="15%" CssClass="tableBorder">
               <asp:Label ID="Label59" runat="server" Text="RCA Description" Font-Bold="true" Font-Size="Medium"></asp:Label>
            </asp:TableHeaderCell>
            <asp:TableHeaderCell Width="8%" CssClass="tableBorder">
               <asp:Label ID="Label60" runat="server" Text="Responsible Manager" Font-Bold="true" Font-Size="Medium"></asp:Label>
            </asp:TableHeaderCell>
          </asp:TableHeaderRow>


         </asp:Table>



       </asp:TableCell>
    </asp:TableRow>


  </asp:Table>

  <div style="width:70%;border:none;text-align:center;color:teal;font-size:medium;">
    <asp:Label ID="lblNumRows" runat="server" Text=""></asp:Label>
  </div>


  </div>

  <asp:HiddenField ID="hdnRegionSSA" runat="server" />
  <asp:HiddenField ID="hdnSelAllSites" runat="server" />
  <asp:HiddenField ID="hdnSiteNGX" runat="server" />
  <asp:HiddenField ID="hdnSiteSAI" runat="server" />
  <asp:HiddenField ID="hdnSiteSTA" runat="server" />
  <asp:HiddenField ID="hdnSiteTUG" runat="server" />

  <asp:HiddenField ID="hdnUnlimited" runat="server" />

  <asp:HiddenField ID="hdnSelAllProc" runat="server" />
  <asp:HiddenField ID="hdnRCAType" runat="server" />

  <asp:HiddenField ID="hdnRCAOpen" runat="server" />
  <asp:HiddenField ID="hdnRCAClosed" runat="server" />
  <asp:HiddenField ID="hdnOlder60days" runat="server" />

  <asp:HiddenField ID="hdnLstProc1" runat="server" />
  <asp:HiddenField ID="hdnLstProc2" runat="server" />
  <asp:HiddenField ID="hdnLstProc3" runat="server" />
  <asp:HiddenField ID="hdnLstProc4" runat="server" />
  <asp:HiddenField ID="hdnLstProc5" runat="server" />
  <asp:HiddenField ID="hdnLstProc6" runat="server" />
  <asp:HiddenField ID="hdnLstProc7" runat="server" />
  <asp:HiddenField ID="hdnLstProc8" runat="server" />
  <asp:HiddenField ID="hdnLstProc9" runat="server" />
  <asp:HiddenField ID="hdnLstProc10" runat="server" />
  <asp:HiddenField ID="hdnLstProc11" runat="server" />
  <asp:HiddenField ID="hdnLstProc12" runat="server" />
  <asp:HiddenField ID="hdnLstProc13" runat="server" />
  <asp:HiddenField ID="hdnLstProc14" runat="server" />
  <asp:HiddenField ID="hdnLstProc15" runat="server" />
  <asp:HiddenField ID="hdnLstProc16" runat="server" />
  <asp:HiddenField ID="hdnLstProc17" runat="server" />
  <asp:HiddenField ID="hdnLstProc18" runat="server" />
  <asp:HiddenField ID="hdnLstProc19" runat="server" />
  <asp:HiddenField ID="hdnLstProc20" runat="server" />
  <asp:HiddenField ID="hdnLstProc21" runat="server" />

  <asp:HiddenField ID = "hdnLstProcChk1" runat="server" />
  <asp:HiddenField ID = "hdnLstProcChk2" runat="server" />
  <asp:HiddenField ID = "hdnLstProcChk3" runat="server" />
  <asp:HiddenField ID = "hdnLstProcChk4" runat="server" />
  <asp:HiddenField ID = "hdnLstProcChk5" runat="server" />
  <asp:HiddenField ID = "hdnLstProcChk6" runat="server" />
  <asp:HiddenField ID = "hdnLstProcChk7" runat="server" />
  <asp:HiddenField ID = "hdnLstProcChk8" runat="server" />
  <asp:HiddenField ID = "hdnLstProcChk9" runat="server" />
  <asp:HiddenField ID = "hdnLstProcChk10" runat="server" />
  <asp:HiddenField ID = "hdnLstProcChk11" runat="server" />
  <asp:HiddenField ID = "hdnLstProcChk12" runat="server" />
  <asp:HiddenField ID = "hdnLstProcChk13" runat="server" />
  <asp:HiddenField ID = "hdnLstProcChk14" runat="server" />
  <asp:HiddenField ID = "hdnLstProcChk15" runat="server" />
  <asp:HiddenField ID = "hdnLstProcChk16" runat="server" />
  <asp:HiddenField ID = "hdnLstProcChk17" runat="server" />
  <asp:HiddenField ID = "hdnLstProcChk18" runat="server" />
  <asp:HiddenField ID = "hdnLstProcChk19" runat="server" />
  <asp:HiddenField ID = "hdnLstProcChk20" runat="server" />
  <asp:HiddenField ID = "hdnLstProcChk21" runat="server" />


  <asp:HiddenField ID = "hdnLstProcTxt1" runat="server" />
  <asp:HiddenField ID = "hdnLstProcTxt2" runat="server" />
  <asp:HiddenField ID = "hdnLstProcTxt3" runat="server" />
  <asp:HiddenField ID = "hdnLstProcTxt4" runat="server" />
  <asp:HiddenField ID = "hdnLstProcTxt5" runat="server" />
  <asp:HiddenField ID = "hdnLstProcTxt6" runat="server" />
  <asp:HiddenField ID = "hdnLstProcTxt7" runat="server" />
  <asp:HiddenField ID = "hdnLstProcTxt8" runat="server" />
  <asp:HiddenField ID = "hdnLstProcTxt9" runat="server" />
  <asp:HiddenField ID = "hdnLstProcTxt10" runat="server" />
  <asp:HiddenField ID = "hdnLstProcTxt11" runat="server" />
  <asp:HiddenField ID = "hdnLstProcTxt12" runat="server" />
  <asp:HiddenField ID = "hdnLstProcTxt13" runat="server" />
  <asp:HiddenField ID = "hdnLstProcTxt14" runat="server" />
  <asp:HiddenField ID = "hdnLstProcTxt15" runat="server" />
  <asp:HiddenField ID = "hdnLstProcTxt16" runat="server" />
  <asp:HiddenField ID = "hdnLstProcTxt17" runat="server" />
  <asp:HiddenField ID = "hdnLstProcTxt18" runat="server" />
  <asp:HiddenField ID = "hdnLstProcTxt19" runat="server" />
  <asp:HiddenField ID = "hdnLstProcTxt20" runat="server" />
  <asp:HiddenField ID = "hdnLstProcTxt21" runat="server" />

<script>
//--https://www.jqueryscript.net/time-clock/Clean-jQuery-Date-Time-Picker-Plugin-datetimepicker.html

  $( function() {
    $("#inpStartDate").datetimepicker({
      //inline: true     always visible
      format:'Y-m-d',
      closeOnDateSelect: true,
      datepicker:true,
      timepicker: false,
      initTime:false,
      //closeOnTimeSelect:true,
    })
  });

   $( function() {
     $("#inpEndDate").datetimepicker({
       //inline: true     always visible
       format: 'Y-m-d',
       closeOnDateSelect: true,
       datepicker: true,
       timepicker: false,
       initTime: false,
       //closeOnTimeSelect:true,
     });
  });


   function pad(number, length)
   {

     var str = '' + number;
     while (str.length < length) {
       str = '0' + str;
     }

     return str;
   }


   showTime();   //initial load
   var i = setInterval(showTime, 60000);


   function showTime()
   {
     //console.log("showTime");
     var d = new Date();
     document.getElementById("todaydate").innerHTML = d.getFullYear() + '-' + pad(d.getMonth(), 2) + '-' + pad(d.getDate(), 2)
                                + ' ' + pad(d.getHours(),2) + ':' + pad(d.getMinutes(),2);
   }


   $(document).ready(function ()
   {
     $("#runMsg").hide();
     $("#runMsg").html("");

     $("#BodyPlaceHolder_cmdRunReport").on("click", function ()
     {
       $("#runMsg").html("Fetching data....");
       $("#runMsg").show();

     });

     $("#BodyPlaceHolder_cboProcessArea").on("change", function () {
       $("#runMsg").html("Fetching data....");
       $("#runMsg").show();

     });


     $("#BodyPlaceHolder_cboRCAStatus").on("change", function () {
       $("#runMsg").html("Fetching data....");
       $("#runMsg").show();
     });

   });   





  //function myFunction()
  //{
  //var ele = document.querySelectorAll('[id^=textbox]');

  //  var x = document.getElementById("tbl").querySelectorAll(".classname");
  //  var i;
  //  for (i = 0; i < x.length; i++) {
  //    x[i].style.backgroundColor = "red";
  //  }
  //}

</script>




<%--
  <div class="container jumbotron">

  jumbotron baby
</div>

--%>

</asp:Content>
