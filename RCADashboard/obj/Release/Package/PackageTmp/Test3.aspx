﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Test3.aspx.cs" Inherits="RCADashboard.Test3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageTitlePlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="server">

  <script src="scripts/jquery-3.4.1.min.js"></script>
  <script src="scripts/bootstrap.min.js"></script>

  <link rel="stylesheet" type="text/css" href="Content/bootstrap.min.css" />

<%--  https: //stackoverflow.com/questions/57811709/how-do-you-change-the-background-color-on-the-active-state-of-a-bootstrap-custom--%>

<style>

  .custom-checkbox .custom-control-input:checked ~ .custom-control-label::before 
 {
      background-color: green!important;
 }

  .custom-checkbox .custom-control-input:not(checked) ~ .custom-control-label::before 
 {
      background-color:darkgoldenrod!important;
 }



</style>
</asp:Content>



<asp:Content ID="Content3" ContentPlaceHolderID="BodyPlaceHolder" runat="server">


<!-- Default unchecked -->
<div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="defaultUnchecked">
    <label class="custom-control-label" for="defaultUnchecked">Default unchecked</label>
</div>


<!-- Default checked -->
<div class="custom-control custom-checkbox">
  <input type="checkbox" class="custom-control-input" id="efaultChecked2" checked>
  <label class="custom-control-label" for="efaultChecked2">Default checked</label>
</div>


<!-- Default indeterminate -->
<div class="custom-control custom-checkbox">
  <input type="checkbox" class="custom-control-input" id="defaultIndeterminate2" checked>
  <label class="custom-control-label" for="defaultIndeterminate2">Default indeterminate</label>
</div>



<!-- Default unchecked disabled -->
<div class="custom-control custom-checkbox">
  <input type="checkbox" class="custom-control-input" id="defaultUncheckedDisabled2" disabled>
  <label class="custom-control-label" for="defaultUncheckedDisabled2">Default unchecked disabled</label>
</div>

<!-- Default checked disabled -->
<div class="custom-control custom-checkbox">
  <input type="checkbox" class="custom-control-input" id="defaultCheckedDisabled2" checked disabled>
  <label class="custom-control-label" for="defaultCheckedDisabled2">Default checked disabled</label>
</div>


<!-- Default inline 1-->
<div class="custom-control custom-checkbox custom-control-inline">
  <input type="checkbox" class="custom-control-input" id="defaultInline1">
  <label class="custom-control-label" for="defaultInline1">1</label>
</div>

<!-- Default inline 2-->
<div class="custom-control custom-checkbox custom-control-inline">
  <input type="checkbox" class="custom-control-input" id="defaultInline2">
  <label class="custom-control-label" for="defaultInline2">2</label>
</div>

<!-- Default inline 3-->
<div class="custom-control custom-checkbox custom-control-inline">
  <input type="checkbox" class="custom-control-input" id="defaultInline3">
  <label class="custom-control-label" for="defaultInline3">3</label>
</div>


  
<script>

  $('#defaultIndeterminate2').prop('indeterminate', true);


   $(document).ready(function ()
   {
     $("#defaultIndeterminate2").on("click", function () {
       if ($('#defaultIndeterminate2').prop("checked") == false) {
         $('#defaultIndeterminate2').prop('indeterminate', true);
       }

     });

   });   






</script>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="FooterPlaceHolder" runat="server">
</asp:Content>
