﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="frmError.aspx.cs" Inherits="RCADashboard.frmError" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageTitlePlaceHolder" runat="server">
  Error Page
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="server">

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="BodyPlaceHolder" runat="server">


   <br />
  <asp:Label id="lblMsg" runat="server" Text=""></asp:Label>
  <br />
  <asp:HyperLink runat="server" id="lnkBackHome" NavigateUrl="~/RCADashboard.aspx" >Back to Home Page</asp:HyperLink>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="FooterPlaceHolder" runat="server">

</asp:Content>


