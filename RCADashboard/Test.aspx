﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Test.aspx.cs" Inherits="RCADashboard.Test" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PageTitlePlaceHolder" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="server">
  
  <script src="scripts/jquery-3.4.1.min.js"></script>
  <script src="scripts/bootstrap.min.js"></script>

  <link rel="stylesheet" type="text/css" href="Content/bootstrap.min.css" />

<style>

/* Red CSS Checkbox approach */
.red-checkbox {
	width: 14px;
	position: relative;
}
.red-checkbox label {
	cursor: pointer;
	position: absolute;
	width: 14px;
	height: 14px;
	top: 0;
  left: 0;
	background: #4cff00;
	border:1px solid #ddd;
}


.red-checkbox label:after {
	opacity: 0.0;
	content: '';
	position: absolute;
	width: 7px;
	height: 5px;
	background: transparent;
	top: 3px;
	left: 3px;
	border: 3px solid red;
	border-top: none;
	border-right: none;

	-webkit-transform: rotate(-45deg);
	-moz-transform: rotate(-45deg);
	-o-transform: rotate(-45deg);
	-ms-transform: rotate(-45deg);
	transform: rotate(-45deg);
}

/**
 * Create the hover event of the tick
 */
.red-checkbox label:hover::after {
	opacity: 0.5;
}

/**
 * Create the checkbox state for the tick
 */
.red-checkbox input[type=checkbox]:checked + label:after {
	opacity: 1;
}


</style>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="BodyPlaceHolder" runat="server">

  <asp:Table runat="server" Width="70%">
    <asp:TableRow>

      <asp:TableCell Width="50%">
          <div class="red-checkbox">
               <input type="checkbox" value="1" id="chk1" name="" />
               <label for="chk1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;123456789</label>
          </div>

      </asp:TableCell>
      <asp:TableCell Width="50%">
          <div class="red-checkbox">
             <input type="checkbox" value="1" id="chk2" name="" />
             <label for="chk2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;abcdefghijjjj</label>
          </div>
      </asp:TableCell>
    </asp:TableRow>

  </asp:Table>

    <div class="red-checkbox">
          <div class="red-checkbox">
               <input type="checkbox" value="1" id="chk3" name="" />
               <label for="chk3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wwwwwwwwwwwww</label>
          </div>
          <div class="red-checkbox">
             <input type="checkbox" value="1" id="chk4" name="" />
             <label for="chk4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0000000000000</label>
          </div>

    </div>




</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="FooterPlaceHolder" runat="server">

</asp:Content>
