﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections.Specialized;


//2020-05-11 Original dashboard.

namespace RCADashboard
{
  public partial class RCADashboard01 : System.Web.UI.Page
  {


    DataTable grid = null;
    DataTable dtTot = null;

    protected void Page_Load(object sender, EventArgs e)
    {

      if (!IsPostBack)
      {
        cFG.LogIt(cFG.GetCurrentIdentity());
        LoadPage();
      }

    } //



    private void LoadPage()
    {

      hdnStartDate.Value = math.getDateNowStr(-7);
      //hdnStartDate.Value = "2019-11-25";
      hdnEndDate.Value = math.getDateNowStr();

      cboProcessArea.Items.Clear();
      cboProcessArea.Items.Add("Process Area");

      cboRCAStatus.Items.Clear();
      cboRCAStatus.Items.Add("RCA Status");

      string sql = "Select b.busprocgrpid, b.busprocname, b.bucode from busprocgrp b order by b.busprocname";

      try
      {
        cORADB.OpenDB();
        DataTable dt = cORADB.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          lstPrcGrp.Items.Add(new ListItem(item["busprocname"].ToString(), item["busprocgrpid"].ToString()));
          //lstPrcGrp.Items[k++].Selected = true;
        }
      }
      catch (Exception er)
      {
        Response.Redirect("frmError.aspx?q=" + er.Message, true);
      }

      finally
      {
        cORADB.CloseDB();
      }

    }//


    private void LoadPage1()
    {

      hdnStartDate.Value = math.getDateNowStr(-7);
      hdnEndDate.Value = math.getDateNowStr();

      string sql = "Select b.busprocgrpid, b.busprocname, b.bucode from busprocgrp b order by b.busprocname";

      try
      {
        cORADB.OpenDB();
        DataTable dt = cORADB.GetDataTable(sql);

        foreach (DataRow item in dt.Rows)
        {
          //lstPrcGrp.Items.Add(new ListItem(item["busprocname"].ToString(), item["busprocgrpid"].ToString()));
          TableCell C1 = new TableCell();
          TableCell C2 = new TableCell();
          TableCell C3 = new TableCell();
          TableCell C4 = new TableCell();
          TableCell C5 = new TableCell();

          C1.CssClass = "tableBorder";
          C2.CssClass = "tableBorder";
          C3.CssClass = "tableBorder";
          C4.CssClass = "tableBorder";
          C5.CssClass = "tableBorder";

          C1.HorizontalAlign = HorizontalAlign.Center;
          C2.HorizontalAlign = HorizontalAlign.Center;
          C3.HorizontalAlign = HorizontalAlign.Center;
          C4.HorizontalAlign = HorizontalAlign.Center;
          C5.HorizontalAlign = HorizontalAlign.Center;


          C1.Text = item["busprocname"].ToString();
          C2.Text = item["site"].ToString();
          C3.Text = item["busprocname"].ToString();



          TableRow R = new TableRow();
          R.CssClass = "tableBorder bodyText";
          R.Cells.Add(C1);
          R.Cells.Add(C2);
          R.Cells.Add(C3);
          R.Cells.Add(C4);
          R.Cells.Add(C5);

          tblItems.Rows.Add(R);

        }
      }
      catch (Exception er)
      {
        Response.Redirect("frmError.aspx?q=" + er.Message, true);
      }

      finally
      {
        cORADB.CloseDB();
      }

    }//


    private bool ValidateForm()
    {

      if (chkUnlimited.Checked == false)      //must supply a date range
      {
        if (math.IsDate(hdnStartDate.Value) == false)
        {
          lblMsg.Text = "Please enter a start date";
          return false;
        }

        if (math.IsDate(hdnEndDate.Value) == false)
        {
          lblMsg.Text = "Please enter an end date";
          return false;
        }
      }


      bool isSelected = chkSiteNGX.Checked || chkSiteSAI.Checked || chkSiteSTA.Checked || chkSiteTUG.Checked;

      if (isSelected == false)
      {
        lblMsg.Text = "Please choose one or more sites";
        return false;
      }

      isSelected = false;
      for (int i = 0; i < lstPrcGrp.Items.Count; i++)
      {
        if (lstPrcGrp.Items[i].Selected)
        {
          isSelected = true;
        }
      }

      if (isSelected == false)
      {
        lblMsg.Text = "Please choose one or more Process area groups";
        return false;
      }

      return isSelected;

    }


    protected void cmdRunReport_Click(object sender, EventArgs e)
    {

      //if (Request.Form.Count > 0)
      //{
      //  string ClientValue = Request.Form["ctl00$BodyPlaceHolder$chk"];
      //}

      //if (Request.Form.Keys.Count > 0)
      //{
      //  NameValueCollection coll = null;
      //  coll = Request.Form;

      //  string ss = "";

      //  // Get names of all forms into a string array.
      //  String[] arr1 = coll.AllKeys;
      //  for (int loop1 = 0; loop1 < arr1.Length; loop1++)
      //  {
      //    ss += arr1[loop1] + "<br / >";
      //  }
      //}


      LoadData("", "");

      //run this only when the button is clicked in order to preserve the listitems
      cboProcessArea.Items.Clear();
      cboProcessArea.Items.Add("Process Area");

      cboRCAStatus.Items.Clear();
      cboRCAStatus.Items.Add("RCA Status");

      if (grid != null)
      {
        foreach (DataRow item in grid.Rows)
        {
          AddToCboProcArea(item["FunctionalDesc"].ToString());
          if (item["status"].ToString() == "")    //because of the left join, there might not be an rca activated
            AddToCboRCAStatus(math.CapWords(item["FiveWhyStatus"].ToString()));
          else
            AddToCboRCAStatus(item["status"].ToString());
        }

      }

    } //


    private void LoadData(string filterArea, string filterRCAStatus)
    {
      string date1 = hdnStartDate.Value;
      string date2 = hdnEndDate.Value;


      lblMsg.Text = "";

      if (ValidateForm() == false)
      {
        return;
      }


      grid = null;          // initialise at start
      dtTot = null;


      try
      {
        if (chkSiteNGX.Checked)
        {
          GetData(cFG.NGX, filterArea, filterRCAStatus);
          GetTotals(cFG.NGX);
        }

        if (chkSiteSAI.Checked)
        {
          GetData(cFG.SAI, filterArea, filterRCAStatus);
          GetTotals(cFG.SAI);
        }

        if (chkSiteSTA.Checked)
        {
          GetData(cFG.STA, filterArea, filterRCAStatus);
          GetTotals(cFG.STA);
        }

        if (chkSiteTUG.Checked)
        {
          GetData(cFG.TUG, filterArea, filterRCAStatus);
          GetTotals(cFG.TUG);
        }

        if (grid.Rows.Count > 0)
          DoReport();

        if (dtTot.Rows.Count > 0)
          DoReportTotals();

        lblNumRows.Text = "# RCA Items " + grid.Rows.Count.ToString();

      }
      catch (Exception er)
      {
        lblMsg.Text = er.Message;
      }

      finally
      {

      }

    } //


    private void GetData(string site, string filterArea, string filterRCAStatus)
    {

      byte cnt = 0;

      grid = grid ?? new DataTable();

      string sql = " Select rca.eventid, E.EventID EventNum, rca.datecreated, rca.manager, br.region, bss.site"
              + " , rca.status, E.FiveWhyStatus, rca.problemdesc"
              + " , bproc.busprocgrpid, bproc.busprocname, bproc.bucode, fa.FunctionalDesc, ActionItemsTot"
              + " , ActionItemsOpen, (TO_DATE(sysdate,'DD-MM-YYYY') - TO_DATE(E.DateFrom,'DD-MM-YYYY')) DurationOpenInDays";

      sql += " from RCA rca,  event E , functionalareas fa , busprocgrp bproc, busregion br, bussite bss";


      //inline  action items total
      sql += ",( Select  count(ap.task) ActionItemsTot , ap.eventid   from RCA rca  "
          + "        left join  event E on  rca.eventid = e.eventid"
          + "        left join rcaactionplan ap on rca.eventid = ap.eventid"
          + "        left join functionalareas fa on fa.flid = e.flid"
          + "        left join busprocgrp bproc on fa.busprocgrpid = bproc.busprocgrpid"
          + "        left join busregion br on bproc.region = br.region"
          + "        left join bussite bss on bproc.site =  bss.site"
          + "       where bproc.region = '" + chkRegionSSA.Text + "'"
          + "       and bproc.site = '" + site + "'"
          + "";

      sql += " and bproc.busprocgrpid IN ( ";

      cnt = 0;
      for (int i = 0; i < lstPrcGrp.Items.Count; i++)
      {
        if (lstPrcGrp.Items[i].Selected)
        {
          if (cnt > 0)
            sql += "," + lstPrcGrp.Items[i].Value;
          else
            sql += lstPrcGrp.Items[i].Value;

          cnt++;
        }
      }

      sql += ")";

      if ((chkUnlimited.Checked == false) && (chkRCAOlder60.Checked == false))
        sql += " and rca.datecreated between TO_DATE('" + math.getDateStrDDMMYYYY(hdnStartDate.Value) + "','DD-MM-YYYY')  and TO_DATE('" + math.getDateEOD_DDMMYYYY(hdnEndDate.Value) + "','DD-MM-YYYY HH24:MI:SS')";


      sql += "    group by ap.eventid";
      sql += " )  ActionItemsTot";

      //end inline action items total





      //inline  action items open
      sql += ",( Select  count(ap.task) ActionItemsOpen , ap.eventid   from RCA rca  "
          + "        left join  event E on  rca.eventid = e.eventid"
          + "        left join rcaactionplan ap on rca.eventid = ap.eventid"
          + "        left join functionalareas fa on fa.flid = e.flid"
          + "        left join busprocgrp bproc on fa.busprocgrpid = bproc.busprocgrpid"
          + "        left join busregion br on bproc.region = br.region"
          + "        left join bussite bss on bproc.site =  bss.site"
          + "       where bproc.region = '" + chkRegionSSA.Text + "'"
          + "       and bproc.site = '" + site + "'"
          + "       and  (upper(ap.status) <> 'CLOSED')"
          + "";

      sql += " and bproc.busprocgrpid IN ( ";

      cnt = 0;
      for (int i = 0; i < lstPrcGrp.Items.Count; i++)
      {
        if (lstPrcGrp.Items[i].Selected)
        {
          if (cnt > 0)
            sql += "," + lstPrcGrp.Items[i].Value;
          else
            sql += lstPrcGrp.Items[i].Value;

          cnt++;
        }
      }

      sql += ")";

      if ((chkUnlimited.Checked == false) && (chkRCAOlder60.Checked == false))
        sql += " and rca.datecreated between TO_DATE('" + math.getDateStrDDMMYYYY(hdnStartDate.Value) + "','DD-MM-YYYY')  and TO_DATE('" + math.getDateEOD_DDMMYYYY(hdnEndDate.Value) + "','DD-MM-YYYY HH24:MI:SS')";

      sql += "    group by ap.eventid";
      sql += " )  ActionItemsOpen";

      //end inline action items open




      // outer query
      sql += " where  e.eventid = rca.eventid(+)"
        + " and E.flid = fa.flid"
        + " and fa.busprocgrpid = bproc.busprocgrpid"
        + " and bproc.region = br.region"
        + " and bproc.site =  bss.site"
        + " and bproc.region = '" + chkRegionSSA.Text + "'"
        + " and bproc.site = '" + site + "'"
        + "";


      if (filterArea != "")
      {
        sql += " And fa.FunctionalDesc = '" + filterArea + "'";
      }

      if (filterRCAStatus != "")
      {
        sql += " and ( (upper(rca.status) = '" + filterRCAStatus.ToUpper() + "') or (upper(E.FIVEWHYSTATUS) = '" + filterRCAStatus.ToUpper() + "') ) ";
      }

      if ((chkUnlimited.Checked == false) && (chkRCAOlder60.Checked == false))
        sql += " and E.DateFrom between TO_DATE('" + math.getDateStrDDMMYYYY(hdnStartDate.Value) + "','DD-MM-YYYY')  and TO_DATE('" + math.getDateEOD_DDMMYYYY(hdnEndDate.Value) + "','DD-MM-YYYY HH24:MI:SS')";

      sql += " and rca.eventid = ActionItemsOpen.eventid(+)";      //join the inner query detailed action items
      sql += " and rca.eventid = ActionItemsTot.eventid(+)";      //join the inner query detailed action items


      sql += " and bproc.busprocgrpid IN ( ";

      cnt = 0;
      for (int i = 0; i < lstPrcGrp.Items.Count; i++)
      {
        if (lstPrcGrp.Items[i].Selected)
        {
          if (cnt > 0)
            sql += "," + lstPrcGrp.Items[i].Value;
          else
            sql += lstPrcGrp.Items[i].Value;

          cnt++;
        }
      }

      sql += ")";


      sql += CheckOptions();



      sql += " order by E.DateFrom";

      try
      {
        cORADB.OpenDB(site);

        cORADB.addDT(sql, grid);        //add to grid

      }
      catch (Exception er)
      {
        throw er;
      }
      finally
      {
        cORADB.CloseDB();
      }

    } // getdata()



    private void GetTotals(string site)
    {

      byte cnt = 0;

      dtTot = dtTot ?? new DataTable();

      string sql = " Select bproc.site";

      //--------------------------------------------------------------------------
      //inner query  for Total TO BE RAISED
      sql += ",(Select count(E.eventid) from Event E, RCA rca,  functionalareas fa , busprocgrp bproc, busregion br, bussite bss"
           + "       Where e.eventid = rca.eventid(+)"
           + "       and E.flid = fa.flid"
           + "       and fa.busprocgrpid = bproc.busprocgrpid"
           + "       and bproc.region = br.region"
           + "       and bproc.site =  bss.site"
           + "       and bproc.region = '" + chkRegionSSA.Text + "'"
           + "       and bproc.site = '" + site + "'"
           + "       and  ( (upper(rca.status) = 'TO BE RAISED') or (upper(E.FIVEWHYSTATUS) = 'TO BE RAISED') )"
           + "";

      sql += " and bproc.busprocgrpid IN ( ";

      cnt = 0;
      for (int i = 0; i < lstPrcGrp.Items.Count; i++)
      {
        if (lstPrcGrp.Items[i].Selected)
        {
          if (cnt > 0)
            sql += "," + lstPrcGrp.Items[i].Value;
          else
            sql += lstPrcGrp.Items[i].Value;

          cnt++;
        }
      }

      sql += ")";

      //if (chkUnlimited.Checked == false)
      //  sql += " and rca.datecreated between TO_DATE('" + math.getDateStrDDMMYYYY(hdnStartDate.Value) + "','DD-MM-YYYY')  and TO_DATE('" + math.getDateEOD_DDMMYYYY(hdnEndDate.Value) + "','DD-MM-YYYY HH24:MI:SS')";


      sql += " ) as  TotRCATOBeRaised";
      //end inner query  for Total TO BE RAISED
      //--------------------------------------------------------------------------


      //--------------------------------------------------------------------------
      //inner query  for Total initiated
      sql += ",(Select count(rca.eventid) from RCA rca, event E , functionalareas fa , busprocgrp bproc, busregion br, bussite bss"
           + "       Where rca.eventid = e.eventid"
           + "       and E.flid = fa.flid"
           + "       and fa.busprocgrpid = bproc.busprocgrpid"
           + "       and bproc.region = br.region"
           + "       and bproc.site =  bss.site"
           + "       and bproc.region = '" + chkRegionSSA.Text + "'"
           + "       and bproc.site = '" + site + "'"
           + "       and upper(rca.status) = 'INITIATED'"
           + "";

      sql += " and bproc.busprocgrpid IN ( ";

      cnt = 0;
      for (int i = 0; i < lstPrcGrp.Items.Count; i++)
      {
        if (lstPrcGrp.Items[i].Selected)
        {
          if (cnt > 0)
            sql += "," + lstPrcGrp.Items[i].Value;
          else
            sql += lstPrcGrp.Items[i].Value;

          cnt++;
        }
      }

      sql += ")";

      //if (chkUnlimited.Checked == false)
      //  sql += " and rca.datecreated between TO_DATE('" + math.getDateStrDDMMYYYY(hdnStartDate.Value) + "','DD-MM-YYYY')  and TO_DATE('" + math.getDateEOD_DDMMYYYY(hdnEndDate.Value) + "','DD-MM-YYYY HH24:MI:SS')";


      sql += " ) as  TotRCAInitiated";
      //end inner query  for Total initiated
      //--------------------------------------------------------------------------


      //--------------------------------------------------------------------------
      //inner query  for Total in progress
      sql += ",(Select count(rca.eventid) from RCA rca, event E , functionalareas fa , busprocgrp bproc, busregion br, bussite bss"
           + "       Where rca.eventid = e.eventid"
           + "       and E.flid = fa.flid"
           + "       and fa.busprocgrpid = bproc.busprocgrpid"
           + "       and bproc.region = br.region"
           + "       and bproc.site =  bss.site"
           + "       and bproc.region = '" + chkRegionSSA.Text + "'"
           + "       and bproc.site = '" + site + "'"
           + "       and upper(rca.status) = 'IN PROGRESS'"
           + "";

      sql += " and bproc.busprocgrpid IN ( ";

      cnt = 0;
      for (int i = 0; i < lstPrcGrp.Items.Count; i++)
      {
        if (lstPrcGrp.Items[i].Selected)
        {
          if (cnt > 0)
            sql += "," + lstPrcGrp.Items[i].Value;
          else
            sql += lstPrcGrp.Items[i].Value;

          cnt++;
        }
      }

      sql += ")";

      //if (chkUnlimited.Checked == false)
      //  sql += " and rca.datecreated between TO_DATE('" + math.getDateStrDDMMYYYY(hdnStartDate.Value) + "','DD-MM-YYYY')  and TO_DATE('" + math.getDateEOD_DDMMYYYY(hdnEndDate.Value) + "','DD-MM-YYYY HH24:MI:SS')";


      sql += " ) as  TotRCAInProgress";
      //end inner query  for Total in progress
      //--------------------------------------------------------------------------


      //--------------------------------------------------------------------------
      //inner query  for Total in Not Required
      sql += ",(Select count(rca.eventid) from RCA rca, event E , functionalareas fa , busprocgrp bproc, busregion br, bussite bss"
           + "       Where rca.eventid = e.eventid"
           + "       and E.flid = fa.flid"
           + "       and fa.busprocgrpid = bproc.busprocgrpid"
           + "       and bproc.region = br.region"
           + "       and bproc.site =  bss.site"
           + "       and bproc.region = '" + chkRegionSSA.Text + "'"
           + "       and bproc.site = '" + site + "'"
           + "       and upper(rca.status) = 'NOT REQUIRED'"
           + "";

      sql += " and bproc.busprocgrpid IN ( ";

      cnt = 0;
      for (int i = 0; i < lstPrcGrp.Items.Count; i++)
      {
        if (lstPrcGrp.Items[i].Selected)
        {
          if (cnt > 0)
            sql += "," + lstPrcGrp.Items[i].Value;
          else
            sql += lstPrcGrp.Items[i].Value;

          cnt++;
        }
      }

      sql += ")";

      //if (chkUnlimited.Checked == false)
      //  sql += " and rca.datecreated between TO_DATE('" + math.getDateStrDDMMYYYY(hdnStartDate.Value) + "','DD-MM-YYYY')  and TO_DATE('" + math.getDateEOD_DDMMYYYY(hdnEndDate.Value) + "','DD-MM-YYYY HH24:MI:SS')";

      sql += " ) as  TotRCANotRequired";
      //end inner query  for Total not required
      //--------------------------------------------------------------------------



      //--------------------------------------------------------------------------
      //inner query  for Total RCA CLosed
      sql += ",(Select count(rca.eventid) from RCA rca, event E , functionalareas fa , busprocgrp bproc, busregion br, bussite bss"
           + "       Where rca.eventid = e.eventid"
           + "       and E.flid = fa.flid"
           + "       and fa.busprocgrpid = bproc.busprocgrpid"
           + "       and bproc.region = br.region"
           + "       and bproc.site =  bss.site"
           + "       and bproc.region = '" + chkRegionSSA.Text + "'"
           + "       and bproc.site = '" + site + "'"
           + "       and upper(rca.status) = 'CLOSED'"
           + "";

      sql += " and bproc.busprocgrpid IN ( ";

      cnt = 0;
      for (int i = 0; i < lstPrcGrp.Items.Count; i++)
      {
        if (lstPrcGrp.Items[i].Selected)
        {
          if (cnt > 0)
            sql += "," + lstPrcGrp.Items[i].Value;
          else
            sql += lstPrcGrp.Items[i].Value;

          cnt++;
        }
      }

      sql += ")";

      //if (chkUnlimited.Checked == false)
      //  sql += " and rca.datecreated between TO_DATE('" + math.getDateStrDDMMYYYY(hdnStartDate.Value) + "','DD-MM-YYYY')  and TO_DATE('" + math.getDateEOD_DDMMYYYY(hdnEndDate.Value) + "','DD-MM-YYYY HH24:MI:SS')";


      sql += " ) as  TotRCAClosed";
      //end inner query  for Total RCA Closed
      //--------------------------------------------------------------------------



      //--------------------------------------------------------------------------
      //inner query for Total Action Items

      sql += ",( Select count(ap.task) tasks from RCA rca"
          + "        left join  event E on  rca.eventid = e.eventid"
           + "       left join rcaactionplan ap on rca.eventid = ap.eventid"
           + "       left join functionalareas fa on fa.flid = e.flid"
           + "       left join busprocgrp bproc on fa.busprocgrpid = bproc.busprocgrpid"
           + "       left join busregion br on bproc.region = br.region"
           + "       left join bussite bss on bproc.site =  bss.site"
           + "       where bproc.region = '" + chkRegionSSA.Text + "'"
           + "       and bproc.site = '" + site + "'"
           + "";

      sql += " and bproc.busprocgrpid IN ( ";

      cnt = 0;
      for (int i = 0; i < lstPrcGrp.Items.Count; i++)
      {
        if (lstPrcGrp.Items[i].Selected)
        {
          if (cnt > 0)
            sql += "," + lstPrcGrp.Items[i].Value;
          else
            sql += lstPrcGrp.Items[i].Value;

          cnt++;
        }
      }

      sql += ")";

      // if (chkUnlimited.Checked == false)
      //  sql += " and rca.datecreated between TO_DATE('" + math.getDateStrDDMMYYYY(hdnStartDate.Value) + "','DD-MM-YYYY')  and TO_DATE('" + math.getDateEOD_DDMMYYYY(hdnEndDate.Value) + "','DD-MM-YYYY HH24:MI:SS')";


      sql += " ) as  TotActionItems";
      //end inner query  for Total action items
      //--------------------------------------------------------------------------


      //--------------------------------------------------------------------------
      //inner query for Total Action Items Closed
      sql += ",( Select count(ap.task) tasks from RCA rca"
           + "       left join  event E on  rca.eventid = e.eventid"
           + "       left join rcaactionplan ap on rca.eventid = ap.eventid"
           + "       left join functionalareas fa on fa.flid = e.flid"
           + "       left join busprocgrp bproc on fa.busprocgrpid = bproc.busprocgrpid"
           + "       left join busregion br on bproc.region = br.region"
           + "       left join bussite bss on bproc.site =  bss.site"
           + "       where bproc.region = '" + chkRegionSSA.Text + "'"
           + "       and bproc.site = '" + site + "'"
           + "       and upper(ap.status) = 'CLOSED'"
           + "";

      sql += " and bproc.busprocgrpid IN ( ";

      cnt = 0;
      for (int i = 0; i < lstPrcGrp.Items.Count; i++)
      {
        if (lstPrcGrp.Items[i].Selected)
        {
          if (cnt > 0)
            sql += "," + lstPrcGrp.Items[i].Value;
          else
            sql += lstPrcGrp.Items[i].Value;

          cnt++;
        }
      }

      sql += ")";

      //if (chkUnlimited.Checked == false)
      //  sql += " and rca.datecreated between TO_DATE('" + math.getDateStrDDMMYYYY(hdnStartDate.Value) + "','DD-MM-YYYY')  and TO_DATE('" + math.getDateEOD_DDMMYYYY(hdnEndDate.Value) + "','DD-MM-YYYY HH24:MI:SS')";


      sql += " ) as  TotActionItemsClosed";
      //------------------------------------------------------------------------------------------------------


      //--------------------------------------------------------------------------
      //inner query  for RCA Average Duration Open in days
      sql += ",(Select Round( AVG(TO_DATE(sysdate,'DD-MM-YYYY') - TO_DATE(rca.datecreated,'DD-MM-YYYY'))  ) AveDurationRCAOpen"
           + " from RCA rca, event E , functionalareas fa , busprocgrp bproc, busregion br, bussite bss"
           + "       Where rca.eventid = e.eventid"
           + "       and E.flid = fa.flid"
           + "       and fa.busprocgrpid = bproc.busprocgrpid"
           + "       and bproc.region = br.region"
           + "       and bproc.site =  bss.site"
           + "       and bproc.region = '" + chkRegionSSA.Text + "'"
           + "       and bproc.site = '" + site + "'"
           + "       and upper(rca.status) <> 'CLOSED'"
           + "";

      sql += " and bproc.busprocgrpid IN ( ";

      cnt = 0;
      for (int i = 0; i < lstPrcGrp.Items.Count; i++)
      {
        if (lstPrcGrp.Items[i].Selected)
        {
          if (cnt > 0)
            sql += "," + lstPrcGrp.Items[i].Value;
          else
            sql += lstPrcGrp.Items[i].Value;

          cnt++;
        }
      }

      sql += ")";

      //if (chkUnlimited.Checked == false)
      //  sql += " and rca.datecreated between TO_DATE('" + math.getDateStrDDMMYYYY(hdnStartDate.Value) + "','DD-MM-YYYY')  and TO_DATE('" + math.getDateEOD_DDMMYYYY(hdnEndDate.Value) + "','DD-MM-YYYY HH24:MI:SS')";


      sql += " ) as  AveDurationRCAOpen";
      //end inner query  for Avarage Duration Open in days
      //--------------------------------------------------------------------------



      //outer query
      sql += " from RCA rca,  event E , functionalareas fa , busprocgrp bproc, busregion br, bussite bss";


      sql += " where rca.eventid = e.eventid"
        + " and E.flid = fa.flid"
        + " and fa.busprocgrpid = bproc.busprocgrpid"
        + " and bproc.region = br.region"
        + " and bproc.site =  bss.site"
        + " and bproc.region = '" + chkRegionSSA.Text + "'"
        + " and bproc.site = '" + site + "'"
        + "";


      sql += " and bproc.busprocgrpid IN ( ";

      cnt = 0;
      for (int i = 0; i < lstPrcGrp.Items.Count; i++)
      {
        if (lstPrcGrp.Items[i].Selected)
        {
          if (cnt > 0)
            sql += "," + lstPrcGrp.Items[i].Value;
          else
            sql += lstPrcGrp.Items[i].Value;

          cnt++;
        }
      }

      sql += ")";

      //if (chkUnlimited.Checked == false)
      //  sql += " and rca.datecreated between TO_DATE('" + math.getDateStrDDMMYYYY(hdnStartDate.Value) + "','DD-MM-YYYY')  and TO_DATE('" + math.getDateEOD_DDMMYYYY(hdnEndDate.Value) + "','DD-MM-YYYY HH24:MI:SS')";


      sql += " Group By bproc.site";

      try
      {
        cORADB.OpenDB(site);

        cORADB.addDT(sql, dtTot);        //add to grid

      }
      catch (Exception er)
      {
        throw er;
      }
      finally
      {
        cORADB.CloseDB();
      }

    } // getTotals()



    private string CheckOptions()
    {
      //the RCA Type selected options 

      string sql = "";

      //check for RCA status
      byte cnt = 0;

      if (chkRCAClosed.Checked == true)
      {
        sql += " and (upper(rca.status) = 'CLOSED'";
        cnt++;
      }

      if (chkRCAOpen.Checked == true)
      {
        if (cnt > 0)
          sql += " or ( (upper(rca.status) <> 'CLOSED') and (upper(rca.status) <> 'NOT REQUIRED') or (upper(E.FIVEWHYSTATUS) = 'TO BE RAISED') )";

        else
          sql += " and ( (upper(rca.status) <> 'CLOSED') and (upper(rca.status) <> 'NOT REQUIRED')  or (upper(E.FIVEWHYSTATUS) = 'TO BE RAISED') ) ";
      }

      if (cnt > 0)
        sql += ")";


      //check for > 60 days not yet closed
      if (chkRCAOlder60.Checked == true)
      {
        sql += " and ( (upper(rca.status) <> 'CLOSED' and (upper(rca.status) <> 'NOT REQUIRED') or (upper(E.FIVEWHYSTATUS) = 'TO BE RAISED') )"
          + " and (TO_DATE(sysdate, 'DD-MM-YYYY') - TO_DATE(DateFrom, 'DD-MM-YYYY HH24:MI:SS')) > 60)";
      }

      return sql;
    }


    private void DoReport()
    {


      string stmp = "";

      try
      {
        foreach (DataRow item in grid.Rows)
        {
          TableCell cRegion = new TableCell();
          TableCell cSite = new TableCell();
          TableCell cPAG = new TableCell();
          TableCell cPA = new TableCell();
          TableCell cEvent = new TableCell();
          TableCell cStatus = new TableCell();
          TableCell cTotActionItems = new TableCell();
          TableCell cTotActionItemsOpen = new TableCell();
          TableCell cDurationOpen = new TableCell();
          TableCell cRCADesc = new TableCell();
          TableCell cManager = new TableCell();

          cRegion.CssClass = "tableBorder";
          cSite.CssClass = "tableBorder";
          cPAG.CssClass = "tableBorder";
          cPA.CssClass = "tableBorder";
          cEvent.CssClass = "tableBorder";
          cStatus.CssClass = "tableBorder";
          cTotActionItems.CssClass = "tableBorder";
          cTotActionItemsOpen.CssClass = "tableBorder";
          cDurationOpen.CssClass = "tableBorder";
          cRCADesc.CssClass = "tableBorder";
          cManager.CssClass = "tableBorder";

          cRegion.HorizontalAlign = HorizontalAlign.Center;
          cSite.HorizontalAlign = HorizontalAlign.Center;
          cPAG.HorizontalAlign = HorizontalAlign.Left;
          cPA.HorizontalAlign = HorizontalAlign.Left;
          cEvent.HorizontalAlign = HorizontalAlign.Center;
          cStatus.HorizontalAlign = HorizontalAlign.Left;
          cTotActionItems.HorizontalAlign = HorizontalAlign.Center;
          cTotActionItemsOpen.HorizontalAlign = HorizontalAlign.Center;
          cDurationOpen.HorizontalAlign = HorizontalAlign.Center;
          cRCADesc.HorizontalAlign = HorizontalAlign.Left;
          cManager.HorizontalAlign = HorizontalAlign.Center;



          cRegion.Text = item["region"].ToString();
          cSite.Text = item["site"].ToString();
          cPAG.Text = item["busprocname"].ToString();
          cPA.Text = item["FunctionalDesc"].ToString();

          if (item["site"].ToString() == cFG.NGX)
          {
            if (string.IsNullOrEmpty(item["EventID"].ToString()) == false)
              cEvent.Text = "<a href=\"http://ngxviis03:83/OEENet/frmRCALand.aspx?EVENTID=" + item["EventID"].ToString() + "\" target=\"_blank\">" + item["EventID"].ToString() + "</a>";
            else
              cEvent.Text = "<a href=\"http://ngxviis03:83/OEENet/frmRCALand.aspx?EVENTID=" + item["EventNum"].ToString() + "\" target=\"_blank\">" + item["EventNum"].ToString() + "</a>";
            //lblRCASysTriggersNGX.Text =  item["TotRCATOBeRaised"].ToString();
            //lblRCAInitiatedNGX.Text = item["TotRCAInitiated"].ToString();
            //lblRCAInProgressNGX.Text = item["TotRCAInProgress"].ToString();
            //lblRCANotRequiredNGX.Text =  item["TotRCANotRequired"].ToString();
            //lblRCAClosedNGX.Text = item["TotRCAClosed"].ToString();
            //lblRCAExpectedNGX.Text = (math.geti(item["TotRCATOBeRaised"].ToString()) + math.geti(item["TotRCAInitiated"].ToString())
            //      + math.geti(item["TotRCAInProgress"].ToString()) + math.geti(item["TotRCAClosed"].ToString())).ToString();

            //lblTotActionItemsNGX.Text = item["TotActionItems"].ToString();
            //lblTotActionItemsClosedNGX.Text = item["TotActionItemsClosed"].ToString();
          }

          if (item["site"].ToString() == cFG.SAI)
          {
            if (string.IsNullOrEmpty(item["EventID"].ToString()) == false)
              cEvent.Text = "<a href=\"http://saivls01:82/OEE/frmRCALand.aspx?EVENTID=" + item["EventID"].ToString() + "\" target=\"_blank\">" + item["EventID"].ToString() + "</a>";
            else
              cEvent.Text = "<a href=\"http://saivls01:82/OEE/frmRCALand.aspx?EVENTID=" + item["EventNum"].ToString() + "\" target=\"_blank\">" + item["EventNum"].ToString() + "</a>";

            //lblRCASysTriggersSAI.Text = item["TotRCATOBeRaised"].ToString();
            //lblRCAInitiatedSAI.Text = item["TotRCAInitiated"].ToString();
            //lblRCAInProgressSAI.Text = item["TotRCAInProgress"].ToString();
            //lblRCANotRequiredSAI.Text = item["TotRCANotRequired"].ToString();
            //lblRCAClosedSAI.Text =   item["TotRCAClosed"].ToString();
            //lblRCAExpectedSAI.Text = (math.geti(item["TotRCATOBeRaised"].ToString()) + math.geti(item["TotRCAInitiated"].ToString())
            //      + math.geti(item["TotRCAInProgress"].ToString()) + math.geti(item["TotRCAClosed"].ToString())).ToString();

            //lblTotActionItemsSAI.Text = item["TotActionItems"].ToString();
            //lblTotActionItemsClosedSAI.Text = item["TotActionItemsClosed"].ToString();
          }

          if (item["site"].ToString() == cFG.STA)
          {
            if (string.IsNullOrEmpty(item["EventID"].ToString()) == false)
              cEvent.Text = "<a href=\"http://stavls02/oee/frmRCALand.aspx?EVENTID=" + item["EventID"].ToString() + "\" target=\"_blank\">" + item["EventID"].ToString() + "</a>";
            else
              cEvent.Text = "<a href=\"http://stavls02/oee/frmRCALand.aspx?EVENTID=" + item["EventNum"].ToString() + "\" target=\"_blank\">" + item["EventNum"].ToString() + "</a>";

            //lblRCASysTriggersSTA.Text = item["TotRCATOBeRaised"].ToString();
            //lblRCAInitiatedSTA.Text = item["TotRCAInitiated"].ToString();
            //lblRCAInProgressSTA.Text = item["TotRCAInProgress"].ToString();
            //lblRCANotRequiredSTA.Text = item["TotRCANotRequired"].ToString();
            //lblRCAClosedSTA.Text = item["TotRCAClosed"].ToString();
            //lblRCAExpectedSTA.Text = (math.geti(item["TotRCATOBeRaised"].ToString()) + math.geti(item["TotRCAInitiated"].ToString())
            //                  + math.geti(item["TotRCAInProgress"].ToString()) + math.geti(item["TotRCAClosed"].ToString())).ToString();
            //lblTotActionItemsSTA.Text = item["TotActionItems"].ToString();
            //lblTotActionItemsClosedSTA.Text = item["TotActionItemsClosed"].ToString();
          }

          if (item["site"].ToString() == cFG.TUG)
          {
            if (string.IsNullOrEmpty(item["EventID"].ToString()) == false)
              cEvent.Text = "<a href=\"http://tugvls17/oeenet/frmRCALand.aspx?EVENTID=" + item["EventID"].ToString() + "\" target=\"_blank\">" + item["EventID"].ToString() + "</a>";
            else
              cEvent.Text = "<a href=\"http://tugvls17/oeenet/frmRCALand.aspx?EVENTID=" + item["EventNum"].ToString() + "\" target=\"_blank\">" + item["EventNum"].ToString() + "</a>";


            //lblRCASysTriggersTUG.Text = item["TotRCATOBeRaised"].ToString();
            //lblRCAInitiatedTUG.Text = item["TotRCAInitiated"].ToString();
            //lblRCAInProgressTUG.Text = item["TotRCAInProgress"].ToString();
            //lblRCANotRequiredTUG.Text = item["TotRCANotRequired"].ToString();
            //lblRCAClosedTUG.Text = item["TotRCAClosed"].ToString();
            //lblRCAExpectedTUG.Text = (math.geti(item["TotRCATOBeRaised"].ToString()) + math.geti(item["TotRCAInitiated"].ToString())
            //                  + math.geti(item["TotRCAInProgress"].ToString()) + math.geti(item["TotRCAClosed"].ToString())).ToString();

            //lblTotActionItemsTUG.Text = item["TotActionItems"].ToString();
            //lblTotActionItemsClosedTUG.Text = item["TotActionItemsClosed"].ToString();
          }

          if (string.IsNullOrEmpty(item["Status"].ToString()) == false)
            cStatus.Text = item["Status"].ToString();
          else
            cStatus.Text = math.CapWords(item["FIVEWHYSTATUS"].ToString());

          cTotActionItems.Text = item["ActionItemsTot"].ToString();
          cTotActionItemsOpen.Text = item["ActionItemsOpen"].ToString();
          cDurationOpen.Text = item["DurationOpenInDays"].ToString();

          stmp = item["problemdesc"].ToString();
          cRCADesc.Text = math.GetStr(ref stmp, 45);        // to fit column

          cManager.Text = item["Manager"].ToString();

          TableRow R = new TableRow();
          R.CssClass = "tableBorder bodyText";
          R.BackColor = System.Drawing.Color.White;
          //R.HorizontalAlign = HorizontalAlign.Center;

          R.Cells.Add(cRegion);
          R.Cells.Add(cSite);
          R.Cells.Add(cPAG);
          R.Cells.Add(cPA);
          R.Cells.Add(cEvent);
          R.Cells.Add(cStatus);
          R.Cells.Add(cTotActionItems);
          R.Cells.Add(cTotActionItemsOpen);
          R.Cells.Add(cDurationOpen);
          R.Cells.Add(cRCADesc);
          R.Cells.Add(cManager);

          tblItems.Rows.Add(R);

        } //loop grid

      }
      catch (Exception er)
      {
        lblMsg.Text = er.Message;
      }


    } //


    private void AddToCboProcArea(string pStr)
    {
      ListItem lst = cboProcessArea.Items.FindByText(pStr);
      if (lst == null)
        cboProcessArea.Items.Add(new ListItem(pStr, pStr));
    } //


    private void AddToCboRCAStatus(string pStr)
    {
      ListItem lst = cboRCAStatus.Items.FindByText(pStr);
      if (lst == null)
        cboRCAStatus.Items.Add(new ListItem(pStr, pStr));
    } //


    private void DoReportTotals()
    {

      lblRCASysTriggersNGX.Text = "";
      lblRCAInitiatedNGX.Text = "";
      lblRCAInProgressNGX.Text = "";
      lblRCANotRequiredNGX.Text = "";
      lblRCAClosedNGX.Text = "";
      lblRCAExpectedNGX.Text = "";
      lblTotActionItemsNGX.Text = "";
      lblTotActionItemsClosedNGX.Text = "";
      lblDurationOpenNGX.Text = "";


      lblRCASysTriggersSAI.Text = "";
      lblRCAInitiatedSAI.Text = "";
      lblRCAInProgressSAI.Text = "";
      lblRCANotRequiredSAI.Text = "";
      lblRCAClosedSAI.Text = "";
      lblRCAExpectedSAI.Text = "";
      lblTotActionItemsSAI.Text = "";
      lblTotActionItemsClosedSAI.Text = "";
      lblDurationOpenSAI.Text = "";


      lblRCASysTriggersSTA.Text = "";
      lblRCAInitiatedSTA.Text = "";
      lblRCAInProgressSTA.Text = "";
      lblRCANotRequiredSTA.Text = "";
      lblRCAClosedSTA.Text = "";
      lblRCAExpectedSTA.Text = "";
      lblTotActionItemsSTA.Text = "";
      lblTotActionItemsClosedSTA.Text = "";
      lblDurationOpenSTA.Text = "";

      lblRCASysTriggersTUG.Text = "";
      lblRCAInitiatedTUG.Text = "";
      lblRCAInProgressTUG.Text = "";
      lblRCANotRequiredTUG.Text = "";
      lblRCAClosedTUG.Text = "";
      lblRCAExpectedTUG.Text = "";
      lblTotActionItemsTUG.Text = "";
      lblTotActionItemsClosedTUG.Text = "";
      lblDurationOpenTUG.Text = "";


      try
      {
        foreach (DataRow item in dtTot.Rows)
        {

          if (item["site"].ToString() == cFG.NGX)
          {

            lblRCASysTriggersNGX.Text = item["TotRCATOBeRaised"].ToString();
            lblRCAInitiatedNGX.Text = item["TotRCAInitiated"].ToString();
            lblRCAInProgressNGX.Text = item["TotRCAInProgress"].ToString();
            lblRCANotRequiredNGX.Text = item["TotRCANotRequired"].ToString();
            lblRCAClosedNGX.Text = item["TotRCAClosed"].ToString();
            lblRCAExpectedNGX.Text = (math.geti(item["TotRCATOBeRaised"].ToString()) + math.geti(item["TotRCAInitiated"].ToString())
                  + math.geti(item["TotRCAInProgress"].ToString()) + math.geti(item["TotRCAClosed"].ToString())).ToString();

            lblTotActionItemsNGX.Text = item["TotActionItems"].ToString();
            lblTotActionItemsClosedNGX.Text = item["TotActionItemsClosed"].ToString();
            lblDurationOpenNGX.Text = item["AveDurationRCAOpen"].ToString();

          }

          if (item["site"].ToString() == cFG.SAI)
          {

            lblRCASysTriggersSAI.Text = item["TotRCATOBeRaised"].ToString();
            lblRCAInitiatedSAI.Text = item["TotRCAInitiated"].ToString();
            lblRCAInProgressSAI.Text = item["TotRCAInProgress"].ToString();
            lblRCANotRequiredSAI.Text = item["TotRCANotRequired"].ToString();
            lblRCAClosedSAI.Text = item["TotRCAClosed"].ToString();
            lblRCAExpectedSAI.Text = (math.geti(item["TotRCATOBeRaised"].ToString()) + math.geti(item["TotRCAInitiated"].ToString())
                  + math.geti(item["TotRCAInProgress"].ToString()) + math.geti(item["TotRCAClosed"].ToString())).ToString();

            lblTotActionItemsSAI.Text = item["TotActionItems"].ToString();
            lblTotActionItemsClosedSAI.Text = item["TotActionItemsClosed"].ToString();
            lblDurationOpenSAI.Text = item["AveDurationRCAOpen"].ToString();
          }

          if (item["site"].ToString() == cFG.STA)
          {

            lblRCASysTriggersSTA.Text = item["TotRCATOBeRaised"].ToString();
            lblRCAInitiatedSTA.Text = item["TotRCAInitiated"].ToString();
            lblRCAInProgressSTA.Text = item["TotRCAInProgress"].ToString();
            lblRCANotRequiredSTA.Text = item["TotRCANotRequired"].ToString();
            lblRCAClosedSTA.Text = item["TotRCAClosed"].ToString();
            lblRCAExpectedSTA.Text = (math.geti(item["TotRCATOBeRaised"].ToString()) + math.geti(item["TotRCAInitiated"].ToString())
                              + math.geti(item["TotRCAInProgress"].ToString()) + math.geti(item["TotRCAClosed"].ToString())).ToString();
            lblTotActionItemsSTA.Text = item["TotActionItems"].ToString();
            lblTotActionItemsClosedSTA.Text = item["TotActionItemsClosed"].ToString();
            lblDurationOpenSTA.Text = item["AveDurationRCAOpen"].ToString();
          }

          if (item["site"].ToString() == cFG.TUG)
          {

            lblRCASysTriggersTUG.Text = item["TotRCATOBeRaised"].ToString();
            lblRCAInitiatedTUG.Text = item["TotRCAInitiated"].ToString();
            lblRCAInProgressTUG.Text = item["TotRCAInProgress"].ToString();
            lblRCANotRequiredTUG.Text = item["TotRCANotRequired"].ToString();
            lblRCAClosedTUG.Text = item["TotRCAClosed"].ToString();
            lblRCAExpectedTUG.Text = (math.geti(item["TotRCATOBeRaised"].ToString()) + math.geti(item["TotRCAInitiated"].ToString())
                              + math.geti(item["TotRCAInProgress"].ToString()) + math.geti(item["TotRCAClosed"].ToString())).ToString();

            lblTotActionItemsTUG.Text = item["TotActionItems"].ToString();
            lblTotActionItemsClosedTUG.Text = item["TotActionItemsClosed"].ToString();
            lblDurationOpenTUG.Text = item["AveDurationRCAOpen"].ToString();
          }


        }

      }
      catch (Exception er)
      {
        lblMsg.Text = er.Message;
      }


    } //ReportTotals


    private void RefreshPage()
    {
      //Response.CacheControl = "no-cache";
      //Response.AddHeader("Pragma", "no-cache");
      //Response.Expires = -1;
      //int y = 0;
      ////Response.Write("<script type='text/javascript'>locaton.reload()</script>");



      //Uri uri = Request.Url;

      //Uri uri2 = Request.UrlReferrer;

      //Response.Redirect(Request.RawUrl + "?" + DateTime.Now.Ticks.ToString(), true);
      Response.Redirect(Request.RawUrl, true);

    }

    protected void cboProcessArea_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (cboProcessArea.SelectedIndex != 0)
        LoadData(cboProcessArea.SelectedItem.Text, "");
    }

    protected void cboRCAStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (cboRCAStatus.SelectedIndex != 0)
        LoadData("", cboRCAStatus.SelectedItem.Text);
    }


  }///
}///




/*
      try
      {
        cORADB.OpenDB();
      }
      catch(Exception er)
      {
         lblMsg.Text = er.Message;
      }

      finally
      {
        cORADB.CloseDB();
      }

*/



/*
        try
      {

      }
      catch(Exception er)
      {
         lblMsg.Text = er.Message;
      }

      finally
      {

      }

*/
