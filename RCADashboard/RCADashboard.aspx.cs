﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections.Specialized;



namespace RCADashboard
{
  public partial class RCADashboard : System.Web.UI.Page
  {


    DataTable grid = null;
    DataTable dtTot = null;

    protected void Page_Load(object sender, EventArgs e)
    {

      if (!IsPostBack)
      {
        cFG.LogIt(cFG.GetCurrentIdentity());
        LoadPage1();
      }

    } //



    //private void LoadPage()
    //{

    //  hdnStartDate.Value = math.getDateNowStr(-7);
    //  //hdnStartDate.Value = "2019-11-25";
    //  hdnEndDate.Value = math.getDateNowStr();

    //  hdnRegionSSA.Value = "checked";
    //  hdnRCAOpen.Value = "checked";

    //  //clear the dropdowns filter in details
    //  cboProcessArea.Items.Clear();
    //  cboProcessArea.Items.Add("Process Area");

    //  cboRCAStatus.Items.Clear();
    //  cboRCAStatus.Items.Add("RCA Status");


    //  //load the business process groups
    //  string sql = "Select b.busprocgrpid, b.busprocname, b.bucode from busprocgrp b order by b.busprocname";

    //  try
    //  {
    //    cORADB.OpenDB();
    //    DataTable dt = cORADB.GetDataTable(sql);
    //    foreach (DataRow item in dt.Rows)
    //    {
    //      //lstPrcGrp.Items.Add(new ListItem(item["busprocname"].ToString(), item["busprocgrpid"].ToString()));
    //      //lstPrcGrp.Items[k++].Selected = true;
    //    }

    //    cORADB.CloseDB();

    //    // LoadTotals();

    //  }
    //  catch (Exception er)
    //  {
    //    Response.Redirect("frmError.aspx?q=" + er.Message, true);
    //  }

    //  finally
    //  {
    //    cORADB.CloseDB();
    //  }

    //}//



    private void LoadPage1()
    {

      int ndx = 1;

      hdnStartDate.Value = math.getDateNowStr(-7);
      //hdnStartDate.Value = "2019-11-25";
      hdnEndDate.Value = math.getDateNowStr();

      hdnRegionSSA.Value = "checked";
      hdnRCAOpen.Value = "checked";

      //clear the dropdowns filter in details
      cboProcessArea.Items.Clear();
      cboProcessArea.Items.Add("Process Area");

      cboRCAStatus.Items.Clear();
      cboRCAStatus.Items.Add("RCA Status");

      string sql = "Select b.busprocgrpid, b.busprocname, b.bucode from busprocgrp b order by b.busprocname";


      try
      {
        //load the business process group check boxes
        cORADB.OpenDB();
        DataTable dt = cORADB.GetDataTable(sql);

        foreach (DataRow item in dt.Rows)
        {
          ndx = math.geti(item["busprocgrpid"].ToString());

          switch (ndx)
          {
            case 1:
              hdnLstProc1.Value = item["busprocgrpid"].ToString();
              hdnLstProcTxt1.Value = item["busprocname"].ToString();
              break;

            case 2:
              hdnLstProc2.Value = item["busprocgrpid"].ToString();
              hdnLstProcTxt2.Value = item["busprocname"].ToString();
              break;

            case 3:
              hdnLstProc3.Value = item["busprocgrpid"].ToString();
              hdnLstProcTxt3.Value = item["busprocname"].ToString();
              break;

            case 4:
              hdnLstProc4.Value = item["busprocgrpid"].ToString();
              hdnLstProcTxt4.Value = item["busprocname"].ToString();
              break;

            case 5:
              hdnLstProc5.Value = item["busprocgrpid"].ToString();
              hdnLstProcTxt5.Value = item["busprocname"].ToString();
              break;

            case 6:
              hdnLstProc6.Value = item["busprocgrpid"].ToString();
              hdnLstProcTxt6.Value = item["busprocname"].ToString();
              break;

            case 7:
              hdnLstProc7.Value = item["busprocgrpid"].ToString();
              hdnLstProcTxt7.Value = item["busprocname"].ToString();
              break;

            case 8:
              hdnLstProc8.Value = item["busprocgrpid"].ToString();
              hdnLstProcTxt8.Value = item["busprocname"].ToString();
              break;

            case 9:
              hdnLstProc9.Value = item["busprocgrpid"].ToString();
              hdnLstProcTxt9.Value = item["busprocname"].ToString();
              break;

            case 10:
              hdnLstProc10.Value = item["busprocgrpid"].ToString();
              hdnLstProcTxt10.Value = item["busprocname"].ToString();
              break;

            case 11:
              hdnLstProc11.Value = item["busprocgrpid"].ToString();
              hdnLstProcTxt11.Value = item["busprocname"].ToString();
              break;

            case 12:
              hdnLstProc12.Value = item["busprocgrpid"].ToString();
              hdnLstProcTxt12.Value = item["busprocname"].ToString();
              break;

            case 13:
              hdnLstProc13.Value = item["busprocgrpid"].ToString();
              hdnLstProcTxt13.Value = item["busprocname"].ToString();
              break;

            case 14:
              hdnLstProc14.Value = item["busprocgrpid"].ToString();
              hdnLstProcTxt14.Value = item["busprocname"].ToString();
              break;

            case 15:
              hdnLstProc15.Value = item["busprocgrpid"].ToString();
              hdnLstProcTxt15.Value = item["busprocname"].ToString();
              break;

            case 16:
              hdnLstProc16.Value = item["busprocgrpid"].ToString();
              hdnLstProcTxt16.Value = item["busprocname"].ToString();
              break;

            case 17:
              hdnLstProc17.Value = item["busprocgrpid"].ToString();
              hdnLstProcTxt17.Value = item["busprocname"].ToString();
              break;

            case 18:
              hdnLstProc18.Value = item["busprocgrpid"].ToString();
              hdnLstProcTxt18.Value = item["busprocname"].ToString();
              break;

            case 19:
              hdnLstProc19.Value = item["busprocgrpid"].ToString();
              hdnLstProcTxt19.Value = item["busprocname"].ToString();
              break;

            case 20:
              hdnLstProc20.Value = item["busprocgrpid"].ToString();
              hdnLstProcTxt20.Value = item["busprocname"].ToString();
              break;

            case 21:
              hdnLstProc21.Value = item["busprocgrpid"].ToString();
              hdnLstProcTxt21.Value = item["busprocname"].ToString();
              break;
          }

        } //for each

        cORADB.CloseDB();


        LoadTotals(false);

      }
      catch (Exception er)
      {
        cFG.LogIt("LoadPage1 " + er.Message);
        Response.Redirect("frmError.aspx?q=" + er.Message, true);
      }

      finally
      {
        cORADB.CloseDB();
      }

    }//


    private bool ValidateForm()
    {

      if (Request.Form["chkRegionSSA"] == null)
      {
        lblMsg.Text = "Please select a region";
        hdnRegionSSA.Value = "";
        return false;
      }



      if (Request.Form["chkUnlimited"] != null)     //must supply a date range
      {
        if (math.IsDate(hdnStartDate.Value) == false)
        {
          lblMsg.Text = "Please enter a start date";
          return false;
        }

        if (math.IsDate(hdnEndDate.Value) == false)
        {
          lblMsg.Text = "Please enter an end date";
          return false;
        }
      }


      bool isSelected = (Request.Form["chkSiteNGX"] != null) || (Request.Form["chkSiteSAI"] != null)
            || (Request.Form["chkSiteSTA"] != null) || (Request.Form["chkSiteTUG"] != null);

      if (isSelected == false)
      {
        lblMsg.Text = "Please choose one or more sites";
        return false;
      }


      isSelected = (hdnLstProcChk1.Value == "checked") || (hdnLstProcChk2.Value == "checked") ||
                   (hdnLstProcChk3.Value == "checked") || (hdnLstProcChk4.Value == "checked") ||
                   (hdnLstProcChk5.Value == "checked") || (hdnLstProcChk6.Value == "checked") ||
                   (hdnLstProcChk7.Value == "checked") || (hdnLstProcChk8.Value == "checked") ||
                   (hdnLstProcChk9.Value == "checked") || (hdnLstProcChk10.Value == "checked") ||
                   (hdnLstProcChk11.Value == "checked") || (hdnLstProcChk12.Value == "checked") ||
                   (hdnLstProcChk13.Value == "checked") || (hdnLstProcChk14.Value == "checked") ||
                   (hdnLstProcChk15.Value == "checked") || (hdnLstProcChk16.Value == "checked") ||
                   (hdnLstProcChk17.Value == "checked") || (hdnLstProcChk18.Value == "checked") ||
                   (hdnLstProcChk19.Value == "checked") || (hdnLstProcChk20.Value == "checked") ||
                   (hdnLstProcChk21.Value == "checked");

      if (isSelected == false)
      {
        lblMsg.Text = "Please choose one or more Process area groups";
        return false;
      }

      return isSelected;
            
    }





    protected void cmdRunReport_Click(object sender, EventArgs e)
    {

      LoadData("", "","");

      //Load the dropdowns filter seletion
      cboProcessArea.Items.Clear();
      cboProcessArea.Items.Add("Process Area");

      cboRCAStatus.Items.Clear();
      cboRCAStatus.Items.Add("RCA Status");


      //populate dropdownlists in the details section
      if (grid != null)
      {

        foreach (DataRow item in grid.Rows)
        {

          AddToCboProcArea(item["FunctionalDesc"].ToString());

          if (item["status"].ToString() == "")    //because of the left join, there might not be an rca activated
            AddToCboRCAStatus(math.CapWords(item["FiveWhyStatus"].ToString()));
          else
            AddToCboRCAStatus(item["status"].ToString());

        }

        LoadTotals( (true && hdnUnlimited.Value == "") );     //load the totals by date range unless unlimited is selected

      } //grid != null

      


    } //


    private void LoadData(string filterArea, string filterRCAStatus, string orderHrs)
    {
      string date1 = hdnStartDate.Value;
      string date2 = hdnEndDate.Value;
      string sRegion = "SSA";

      lblMsg.Text = "";


      //GetForm();

      hdnRegionSSA.Value = "";
      if (Request.Form["chkRegionSSA"] != null)
        if (Request.Form["chkRegionSSA"] == "SSA")
          hdnRegionSSA.Value = "checked";


      hdnSelAllSites.Value = "";
      if (Request.Form["chkSelAllSites"] != null)
        if (Request.Form["chkSelAllSites"] == "on")
          hdnSelAllSites.Value = "checked";

      hdnSiteNGX.Value = "";
      if (Request.Form["chkSiteNGX"] != null)
        if (Request.Form["chkSiteNGX"] == "NGX")
          hdnSiteNGX.Value = "checked";

      hdnSiteSAI.Value = "";
      if (Request.Form["chkSiteSAI"] != null)
        if (Request.Form["chkSiteSAI"] == "SAI")
          hdnSiteSAI.Value = "checked";


      hdnSiteSTA.Value = "";
      if (Request.Form["chkSiteSTA"] != null)
        if (Request.Form["chkSiteSTA"] == "STA")
          hdnSiteSTA.Value = "checked";


      hdnSiteTUG.Value = "";
      if (Request.Form["chkSiteTUG"] != null)
        if (Request.Form["chkSiteTUG"] == "TUG")
          hdnSiteTUG.Value = "checked";

      hdnUnlimited.Value = "";
      if (Request.Form["chkUnlimited"] != null)
        if (Request.Form["chKUnlimited"] == "Unlimited")
          hdnUnlimited.Value = "checked";


      hdnSelAllProc.Value = "";
      if (Request.Form["chkSelAllProc"] != null)
        if (Request.Form["chkSelAllProc"] == "SelAllProc")
          hdnSelAllProc.Value = "checked";


      hdnRCAType.Value = "";
      if (Request.Form["chkRCAType"] != null)
        if (Request.Form["chkRCAType"] == "RCAType")
          hdnRCAType.Value = "checked";

      hdnRCAOpen.Value = "";
      if (Request.Form["chkRCAOpen"] != null)
        if (Request.Form["chkRCAOpen"] == "RCAOpen")
          hdnRCAOpen.Value = "checked";


      hdnRCAClosed.Value = "";
      if (Request.Form["chkRCAClosed"] != null)
        if (Request.Form["chkRCAClosed"] == "RCAClosed")
          hdnRCAClosed.Value = "checked";

      hdnOlder60days.Value = "";
      if (Request.Form["chkRCAOlder60"] != null)
        if (Request.Form["chkRCAOlder60"] == "older60")
          hdnOlder60days.Value = "checked";

      hdnLstProcChk1.Value = "";
      if (Request.Form["chkPrcGrp1"] != null)
        if (Request.Form["chkPrcGrp1"] == "1")
          hdnLstProcChk1.Value = "checked";

      hdnLstProcChk2.Value = "";
      if (Request.Form["chkPrcGrp2"] != null)
        if (Request.Form["chkPrcGrp2"] == "2")
          hdnLstProcChk2.Value = "checked";

      hdnLstProcChk3.Value = "";
      if (Request.Form["chkPrcGrp3"] != null)
        if (Request.Form["chkPrcGrp3"] == "3")
          hdnLstProcChk3.Value = "checked";

      hdnLstProcChk4.Value = "";
      if (Request.Form["chkPrcGrp4"] != null)
        if (Request.Form["chkPrcGrp4"] == "4")
          hdnLstProcChk4.Value = "checked";

      hdnLstProcChk5.Value = "";
      if (Request.Form["chkPrcGrp5"] != null)
        if (Request.Form["chkPrcGrp5"] == "5")
          hdnLstProcChk5.Value = "checked";


      hdnLstProcChk6.Value = "";
      if (Request.Form["chkPrcGrp6"] != null)
        if (Request.Form["chkPrcGrp6"] == "6")
          hdnLstProcChk6.Value = "checked";

      hdnLstProcChk7.Value = "";
      if (Request.Form["chkPrcGrp7"] != null)
        if (Request.Form["chkPrcGrp7"] == "7")
          hdnLstProcChk7.Value = "checked";

      hdnLstProcChk8.Value = "";
      if (Request.Form["chkPrcGrp8"] != null)
        if (Request.Form["chkPrcGrp8"] == "8")
          hdnLstProcChk8.Value = "checked";

      hdnLstProcChk9.Value = "";
      if (Request.Form["chkPrcGrp9"] != null)
        if (Request.Form["chkPrcGrp9"] == "9")
          hdnLstProcChk9.Value = "checked";

      hdnLstProcChk10.Value = "";
      if (Request.Form["chkPrcGrp10"] != null)
        if (Request.Form["chkPrcGrp10"] == "10")
          hdnLstProcChk10.Value = "checked";

      hdnLstProcChk11.Value = "";
      if (Request.Form["chkPrcGrp11"] != null)
        if (Request.Form["chkPrcGrp11"] == "11")
          hdnLstProcChk11.Value = "checked";

      hdnLstProcChk12.Value = "";
      if (Request.Form["chkPrcGrp12"] != null)
        if (Request.Form["chkPrcGrp12"] == "12")
          hdnLstProcChk12.Value = "checked";

      hdnLstProcChk13.Value = "";
      if (Request.Form["chkPrcGrp13"] != null)
        if (Request.Form["chkPrcGrp13"] == "13")
          hdnLstProcChk13.Value = "checked";

      hdnLstProcChk14.Value = "";
      if (Request.Form["chkPrcGrp14"] != null)
        if (Request.Form["chkPrcGrp14"] == "14")
          hdnLstProcChk14.Value = "checked";

      hdnLstProcChk15.Value = "";
      if (Request.Form["chkPrcGrp15"] != null)
        if (Request.Form["chkPrcGrp15"] == "15")
          hdnLstProcChk15.Value = "checked";

      hdnLstProcChk16.Value = "";
      if (Request.Form["chkPrcGrp16"] != null)
        if (Request.Form["chkPrcGrp16"] == "16")
          hdnLstProcChk16.Value = "checked";

      hdnLstProcChk17.Value = "";
      if (Request.Form["chkPrcGrp17"] != null)
        if (Request.Form["chkPrcGrp17"] == "17")
          hdnLstProcChk17.Value = "checked";

      hdnLstProcChk18.Value = "";
      if (Request.Form["chkPrcGrp18"] != null)
        if (Request.Form["chkPrcGrp18"] == "18")
          hdnLstProcChk18.Value = "checked";

      hdnLstProcChk19.Value = "";
      if (Request.Form["chkPrcGrp19"] != null)
        if (Request.Form["chkPrcGrp19"] == "19")
          hdnLstProcChk19.Value = "checked";

      hdnLstProcChk20.Value = "";
      if (Request.Form["chkPrcGrp20"] != null)
        if (Request.Form["chkPrcGrp20"] == "20")
          hdnLstProcChk20.Value = "checked";

      hdnLstProcChk21.Value = "";
      if (Request.Form["chkPrcGrp21"] != null)
        if (Request.Form["chkPrcGrp21"] == "21")
          hdnLstProcChk21.Value = "checked";


      if (ValidateForm() == false)
      {   //return to the html the previous state  ie checkboxes will remain ticked
        return;
      }




      grid = null;          // initialise at start


      try
      {
        if (hdnSiteNGX.Value == "checked")
        {
          GetData(cFG.NGX, sRegion, filterArea, filterRCAStatus, hdnUnlimited.Value, hdnRCAOpen.Value, hdnRCAClosed.Value
                      , hdnOlder60days.Value, orderHrs);
        }

        if (hdnSiteSAI.Value == "checked")
        {
          GetData(cFG.SAI, sRegion, filterArea, filterRCAStatus,hdnUnlimited.Value, hdnRCAOpen.Value, hdnRCAClosed.Value
                    , hdnOlder60days.Value, orderHrs);
        }

        if (hdnSiteSTA.Value == "checked")
        {
          GetData(cFG.STA, sRegion, filterArea, filterRCAStatus, hdnUnlimited.Value, hdnRCAOpen.Value, hdnRCAClosed.Value
                    , hdnOlder60days.Value, orderHrs);
        }

        if (hdnSiteTUG.Value == "checked")
        {
          GetData(cFG.TUG, sRegion, filterArea, filterRCAStatus, hdnUnlimited.Value, hdnRCAOpen.Value, hdnRCAClosed.Value
                  , hdnOlder60days.Value, orderHrs);
        }

        if (grid.Rows.Count > 0)
          DoReport();


        lblNumRows.Text = "# RCA Items " + grid.Rows.Count.ToString();

      }
      catch (Exception er)
      {
        lblMsg.Text = er.Message;
      }

      finally
      {

      }

    } //


    private void LoadTotals(bool byDate)
    {

      string sRegion = "SSA";
      //load RCA totals at startup 

      
      dtTot = null;

      try
      {
        GetTotals(cFG.NGX,sRegion, byDate);
        GetTotals(cFG.SAI, sRegion, byDate);
        GetTotals(cFG.STA, sRegion, byDate);
        GetTotals(cFG.TUG, sRegion, byDate);
        if (dtTot.Rows.Count > 0)   
          DoReportTotals();
      }
      catch (Exception er)
      {
        lblMsg.Text = er.Message;
      }

      finally
      {

      }

    } //



    private void GetData(string site, string sRegion, string filterArea,  string  filterRCAStatus
            , string sUnlimited, string sOpen, string sClosed, string sOlder60days, string orderHrs)
    {


      grid = grid ?? new DataTable();

      string sql = " Select E.EventID EventNum, rca.eventid,  rca.datecreated, rca.manager, br.region, bss.site"
              + " , rca.status, E.FiveWhyStatus, rca.problemdesc"
              + " , bproc.busprocgrpid, bproc.busprocname, bproc.bucode, fa.FunctionalDesc, ActionItemsTot, ActionItemsOpen"
              //+ " , (trunc(sysdate) - trunc(E.DateFrom)) DurationOpenInDays"
              + " , decode(upper(rca.status), 'CLOSED', (trunc(rca.dateclosed) - trunc(E.DateFrom) ) ,  (trunc(sysdate) - trunc(E.DateFrom)) ) DurationOpenInDays"
              + " , Round((e.dateto - e.datefrom) * 1440 / 60,2) as dtHrs"
              + " from RCA rca,  event E , functionalareas fa , busprocgrp bproc, busregion br, bussite bss";


      //inline  action items total
      //sql += ",( Select  count(ap.task) ActionItemsTot , ap.eventid   from RCA rca  "
      //    + "        left join  event E on  rca.eventid = e.eventid"
      //    + "        left join rcaactionplan ap on rca.eventid = ap.eventid"
      //    + "        left join functionalareas fa on fa.flid = e.flid"
      //    + "        left join busprocgrp bproc on fa.busprocgrpid = bproc.busprocgrpid"
      //    + "        left join busregion br on bproc.region = br.region"
      //    + "        left join bussite bss on bproc.site =  bss.site"
      //    + "       where bproc.region = '" + sRegion + "'"
      //    + "       and bproc.site = '" + site + "'"
      //    + "";

      //2022-07-19 inline  action items total    -- now using RCAPROBCAUSESITEMS table
      sql += ",( Select  count(ap.task) ActionItemsTot , ap.eventid   from RCA rca  "
          + "        left join  event E on  rca.eventid = e.eventid"
          + "        left join rcaprobcausesitems ap on rca.eventid = ap.eventid"       //new table
          + "        left join functionalareas fa on fa.flid = e.flid"
          + "        left join busprocgrp bproc on fa.busprocgrpid = bproc.busprocgrpid"
          + "        left join busregion br on bproc.region = br.region"
          + "        left join bussite bss on bproc.site =  bss.site"
          + "       where bproc.region = '" + sRegion + "'"
          + "       and bproc.site = '" + site + "'"
          + "";

      sql += GetAreas();

      if ( (sUnlimited == "") && (sOlder60days == ""))
        sql += " and rca.datecreated between TO_DATE('" + math.getDateStrDDMMYYYY(hdnStartDate.Value) + "','DD-MM-YYYY')  and TO_DATE('" + math.getDateEOD_DDMMYYYY(hdnEndDate.Value) + "','DD-MM-YYYY HH24:MI:SS')";

      sql += "    group by ap.eventid";
      sql += " )  ActionItemsTot";

      //end inline action items total


      ////inline  action items open
      //sql += ",( Select  count(ap.task) ActionItemsOpen , ap.eventid   from RCA rca  "
      //    + "        left join  event E on  rca.eventid = e.eventid"
      //    + "        left join rcaactionplan ap on rca.eventid = ap.eventid"
      //    + "        left join functionalareas fa on fa.flid = e.flid"
      //    + "        left join busprocgrp bproc on fa.busprocgrpid = bproc.busprocgrpid"
      //    + "        left join busregion br on bproc.region = br.region"
      //    + "        left join bussite bss on bproc.site =  bss.site"
      //    + "       where bproc.region = '" + sRegion + "'"
      //    + "       and bproc.site = '" + site + "'"
      //    + "       and  (upper(ap.status) <> 'CLOSED')"
      //    + "";

      //2022-07-19
      sql += ",( Select  count(ap.task) ActionItemsOpen , ap.eventid   from RCA rca  "
          + "        left join  event E on  rca.eventid = e.eventid"
          + "        left join rcaprobcausesitems ap on rca.eventid = ap.eventid"       //new
          + "        left join functionalareas fa on fa.flid = e.flid"
          + "        left join busprocgrp bproc on fa.busprocgrpid = bproc.busprocgrpid"
          + "        left join busregion br on bproc.region = br.region"
          + "        left join bussite bss on bproc.site =  bss.site"
          + "       where bproc.region = '" + sRegion + "'"
          + "       and bproc.site = '" + site + "'"
          + "       and  (upper(ap.status) <> 'CLOSED')"
          + "";



      sql += GetAreas();

      if ((sUnlimited == "") && (sOlder60days == ""))
        sql += " and rca.datecreated between TO_DATE('" + math.getDateStrDDMMYYYY(hdnStartDate.Value) + "','DD-MM-YYYY')  and TO_DATE('" + math.getDateEOD_DDMMYYYY(hdnEndDate.Value) + "','DD-MM-YYYY HH24:MI:SS')"; 

      sql += "    group by ap.eventid";
      sql += " )  ActionItemsOpen";

      //end inline action items open




      // outer query
      sql += " where  e.eventid = rca.eventid(+)"
        + " and E.flid = fa.flid"
        + " and fa.busprocgrpid = bproc.busprocgrpid"
        + " and bproc.region = br.region"
        + " and bproc.site =  bss.site"
        + " and bproc.region = '" + sRegion + "'"
        + " and bproc.site = '" + site + "'"
        + "";


      if (filterArea != "")
      {
        sql += " And fa.FunctionalDesc = '" + filterArea + "'";
      }

      if (filterRCAStatus != "")
      {
        sql += " and ( (upper(rca.status) = '" + filterRCAStatus.ToUpper() + "') or (upper(E.FIVEWHYSTATUS) = '" + filterRCAStatus.ToUpper() + "') ) ";
      }

      if ((sUnlimited == "") && (sOlder60days == ""))
        sql += " and E.DateFrom between TO_DATE('" + math.getDateStrDDMMYYYY(hdnStartDate.Value) + "','DD-MM-YYYY')  and TO_DATE('" + math.getDateEOD_DDMMYYYY(hdnEndDate.Value) + "','DD-MM-YYYY HH24:MI:SS')";

      sql += " and E.eventid = ActionItemsOpen.eventid(+)";      //join the inner query detailed action items
      sql += " and E.eventid = ActionItemsTot.eventid(+)";      //join the inner query detailed action items

      sql += GetAreas();

      sql += CheckOptions(sOpen,sClosed,sOlder60days);

      if (orderHrs == "")
        sql += " order by E.DateFrom";
      else
        sql += " order by dtHrs " + orderHrs;

      try
      {
        cORADB.OpenDB(site);

        cORADB.addDT(sql, grid);        //add to grid
          
      }
      catch (Exception er)
      {
        throw er;
      }
      finally
      {
        cORADB.CloseDB();
      }

    } // getdata()



    private void GetTotals(string site, string sRegion, bool byDate)
    {

      //cFG.LogIt("GetTotals() " + site  +  " " + sRegion);

      dtTot = dtTot ?? new DataTable();

      string sql = " Select bproc.site";

      //--------------------------------------------------------------------------
      //inner query  for Total TO BE RAISED
      sql += ",(Select count(E.eventid) from Event E, RCA rca,  functionalareas fa , busprocgrp bproc, busregion br, bussite bss"
           + "       Where e.eventid = rca.eventid(+)"
           + "       and E.flid = fa.flid"
           + "       and fa.busprocgrpid = bproc.busprocgrpid"
           + "       and bproc.region = br.region"
           + "       and bproc.site =  bss.site"
           + "       and bproc.region = '" + sRegion + "'"
           + "       and bproc.site = '" + site + "'"
           + "       and  ( (upper(rca.status) = 'TO BE RAISED') or (upper(E.FIVEWHYSTATUS) = 'TO BE RAISED') )";


      if (byDate)
      {
        sql += " and e.datefrom between TO_DATE('" + math.getDateStrDDMMYYYY(hdnStartDate.Value) + "','DD-MM-YYYY')  and TO_DATE('" + math.getDateEOD_DDMMYYYY(hdnEndDate.Value) + "','DD-MM-YYYY HH24:MI:SS')";
        sql += GetAreas();
      }
      sql += " ) as  TotRCATOBeRaised";


      //end inner query  for Total TO BE RAISED
      //--------------------------------------------------------------------------


      //--------------------------------------------------------------------------
      //inner query  for Total initiated
      sql += ",(Select count(E.eventid) from event E , RCA rca , functionalareas fa , busprocgrp bproc, busregion br, bussite bss"
         + "       Where e.eventid = rca.eventid(+)"
         + "       and E.flid = fa.flid"
         + "       and fa.busprocgrpid = bproc.busprocgrpid"
         + "       and bproc.region = br.region"
         + "       and bproc.site =  bss.site"
         + "       and bproc.region = '" + sRegion + "'"
         + "       and bproc.site = '" + site + "'"
         + "       and  ( (upper(rca.status) = 'INITIATED') or (upper(E.FIVEWHYSTATUS) = 'INITIATED') )";

      if (byDate)
      {
        sql += " and e.datefrom between TO_DATE('" + math.getDateStrDDMMYYYY(hdnStartDate.Value) + "','DD-MM-YYYY')  and TO_DATE('" + math.getDateEOD_DDMMYYYY(hdnEndDate.Value) + "','DD-MM-YYYY HH24:MI:SS')";
        sql += GetAreas();
      }
      sql += " ) as  TotRCAInitiated";
      //end inner query  for Total initiated
      //--------------------------------------------------------------------------


      //--------------------------------------------------------------------------
      //inner query  for Total in progress
      sql += ",(Select count(E.eventid) from event E, RCA rca, functionalareas fa , busprocgrp bproc, busregion br, bussite bss"
           + "       Where e.eventid = rca.eventid(+)"
           + "       and E.flid = fa.flid"
           + "       and fa.busprocgrpid = bproc.busprocgrpid"
           + "       and bproc.region = br.region"
           + "       and bproc.site =  bss.site"
           + "       and bproc.region = '" + sRegion + "'"
           + "       and bproc.site = '" + site + "'"
           + "       and  ( (upper(rca.status) = 'IN PROGRESS') or (upper(E.FIVEWHYSTATUS) = 'IN PROGRESS') )";

      if (byDate)
      {
        sql += " and e.datefrom between TO_DATE('" + math.getDateStrDDMMYYYY(hdnStartDate.Value) + "','DD-MM-YYYY')  and TO_DATE('" + math.getDateEOD_DDMMYYYY(hdnEndDate.Value) + "','DD-MM-YYYY HH24:MI:SS')";
        sql += GetAreas();
      }

      sql += " ) as  TotRCAInProgress";
      //end inner query  for Total in progress
      //--------------------------------------------------------------------------


      //--------------------------------------------------------------------------
      //inner query  for Total in Not Required
      sql += ",(Select count(E.eventid) from event E, RCA rca, functionalareas fa , busprocgrp bproc, busregion br, bussite bss"
           + "       Where e.eventid = rca.eventid(+)"
           + "       and E.flid = fa.flid"
           + "       and fa.busprocgrpid = bproc.busprocgrpid"
           + "       and bproc.region = br.region"
           + "       and bproc.site =  bss.site"
           + "       and bproc.region = '" + sRegion + "'"
           + "       and bproc.site = '" + site + "'"
           + "       and  ( (upper(rca.status) = 'NOT REQUIRED') or (upper(E.FIVEWHYSTATUS) = 'NOT REQUIRED') )";

      if (byDate)
      {
        sql += " and e.datefrom between TO_DATE('" + math.getDateStrDDMMYYYY(hdnStartDate.Value) + "','DD-MM-YYYY')  and TO_DATE('" + math.getDateEOD_DDMMYYYY(hdnEndDate.Value) + "','DD-MM-YYYY HH24:MI:SS')";
        sql += GetAreas();
      }
      sql += " ) as  TotRCANotRequired";
      //end inner query  for Total not required
      //--------------------------------------------------------------------------



      //--------------------------------------------------------------------------
      //inner query  for Total RCA CLosed
      sql += ",(Select count(E.eventid) from event E, RCA rca, functionalareas fa , busprocgrp bproc, busregion br, bussite bss"
           + "       Where e.eventid = rca.eventid(+)"
           + "       and E.flid = fa.flid"
           + "       and fa.busprocgrpid = bproc.busprocgrpid"
           + "       and bproc.region = br.region"
           + "       and bproc.site =  bss.site"
           + "       and bproc.region = '" + sRegion + "'"
           + "       and bproc.site = '" + site + "'"
           + "       and  ( (upper(rca.status) = 'CLOSED') or (upper(E.FIVEWHYSTATUS) = 'CLOSED') )";

      if (byDate)
      {
        sql += " and e.datefrom between TO_DATE('" + math.getDateStrDDMMYYYY(hdnStartDate.Value) + "','DD-MM-YYYY')  and TO_DATE('" + math.getDateEOD_DDMMYYYY(hdnEndDate.Value) + "','DD-MM-YYYY HH24:MI:SS')";
        sql += GetAreas();
      }
      sql += " ) as  TotRCAClosed";
      //end inner query  for Total RCA Closed
      //--------------------------------------------------------------------------



      //--------------------------------------------------------------------------
      //inner query for Total Action Items

      sql += ",( Select count(ap.task) tasks from RCA rca"
          + "        left join  event E on  rca.eventid = e.eventid"
           + "       left join rcaprobcausesitems ap on rca.eventid = ap.eventid"         //new , was rcaactionplan
           + "       left join functionalareas fa on fa.flid = e.flid"
           + "       left join busprocgrp bproc on fa.busprocgrpid = bproc.busprocgrpid"
           + "       left join busregion br on bproc.region = br.region"
           + "       left join bussite bss on bproc.site =  bss.site"
           + "       where bproc.region = '" + sRegion + "'"
           + "       and bproc.site = '" + site + "'";

      if (byDate)
      {
        sql += " and e.datefrom between TO_DATE('" + math.getDateStrDDMMYYYY(hdnStartDate.Value) + "','DD-MM-YYYY')  and TO_DATE('" + math.getDateEOD_DDMMYYYY(hdnEndDate.Value) + "','DD-MM-YYYY HH24:MI:SS')";
        sql += GetAreas();
      }
      sql += " ) as  TotActionItems";
      //end inner query  for Total action items
      //--------------------------------------------------------------------------


      //--------------------------------------------------------------------------
      //inner query for Total Action Items Closed
      sql += ",( Select count(ap.task) tasks from RCA rca"
           + "       left join  event E on  rca.eventid = e.eventid"
           + "       left join rcaprobcausesitems ap on rca.eventid = ap.eventid"       //new  was rcaactionplan
           + "       left join functionalareas fa on fa.flid = e.flid"
           + "       left join busprocgrp bproc on fa.busprocgrpid = bproc.busprocgrpid"
           + "       left join busregion br on bproc.region = br.region"
           + "       left join bussite bss on bproc.site =  bss.site"
           + "       where bproc.region = '" + sRegion + "'"
           + "       and bproc.site = '" + site + "'"
           + "       and upper(ap.status) = 'CLOSED'";

      if (byDate)
      {
        sql += " and e.datefrom between TO_DATE('" + math.getDateStrDDMMYYYY(hdnStartDate.Value) + "','DD-MM-YYYY')  and TO_DATE('" + math.getDateEOD_DDMMYYYY(hdnEndDate.Value) + "','DD-MM-YYYY HH24:MI:SS')";
        sql += GetAreas();
      }
      sql += " ) as  TotActionItemsClosed";
      //------------------------------------------------------------------------------------------------------


      //--------------------------------------------------------------------------
      //inner query  for RCA Average Duration Open in days
      sql += ",(Select Round( AVG(TO_DATE(sysdate,'DD-MM-YYYY') - TO_DATE(rca.datecreated,'DD-MM-YYYY'))  ) AveDurationRCAOpen"
           + " from RCA rca, event E , functionalareas fa , busprocgrp bproc, busregion br, bussite bss"
           + "       Where rca.eventid = e.eventid"
           + "       and E.flid = fa.flid"
           + "       and fa.busprocgrpid = bproc.busprocgrpid"
           + "       and bproc.region = br.region"
           + "       and bproc.site =  bss.site"
           + "       and bproc.region = '" + sRegion + "'"
           + "       and bproc.site = '" + site + "'"
           + "       and upper(rca.status) <> 'CLOSED'";

      if (byDate)
      {
        sql += " and e.datefrom between TO_DATE('" + math.getDateStrDDMMYYYY(hdnStartDate.Value) + "','DD-MM-YYYY')  and TO_DATE('" + math.getDateEOD_DDMMYYYY(hdnEndDate.Value) + "','DD-MM-YYYY HH24:MI:SS')";
        sql += GetAreas();
      }
      sql += " ) as  AveDurationRCAOpen";
      //end inner query  for Avarage Duration Open in days
      //--------------------------------------------------------------------------


      //outer query
      sql += " from RCA rca,  event E , functionalareas fa , busprocgrp bproc, busregion br, bussite bss";


      sql += " where e.eventid = rca.eventid(+)"
        + " and E.flid = fa.flid"
        + " and fa.busprocgrpid = bproc.busprocgrpid"
        + " and bproc.region = br.region"
        + " and bproc.site =  bss.site"
        + " and bproc.region = '" + sRegion + "'"
        + " and bproc.site = '" + site + "'";

      if (byDate)
      {
        sql += " and e.datefrom between TO_DATE('" + math.getDateStrDDMMYYYY(hdnStartDate.Value) + "','DD-MM-YYYY')  and TO_DATE('" + math.getDateEOD_DDMMYYYY(hdnEndDate.Value) + "','DD-MM-YYYY HH24:MI:SS')";
        sql += GetAreas();
      }

        sql += " Group By bproc.site";

      try
      {
        cORADB.OpenDB(site);

        cORADB.addDT(sql, dtTot);        //add to grid

      }
      catch (Exception er)
      {
        cFG.LogIt("GetTotals() " + er.Message);
        throw er;
      }
      finally
      {
        cORADB.CloseDB();
      }

    } // getTotals()


    private string GetAreas()
    {

      string sql = " and bproc.busprocgrpid IN ( ";

      if (hdnLstProcChk1.Value == "checked")
          sql += hdnLstProc1.Value + ",";

      if (hdnLstProcChk2.Value == "checked")
        sql += hdnLstProc2.Value + ",";

      if (hdnLstProcChk3.Value == "checked")
        sql += hdnLstProc3.Value + ",";

      if (hdnLstProcChk4.Value == "checked")
        sql += hdnLstProc4.Value + ",";

      if (hdnLstProcChk5.Value == "checked")
        sql += hdnLstProc5.Value + ",";

      if (hdnLstProcChk6.Value == "checked")
        sql += hdnLstProc6.Value + ",";

      if (hdnLstProcChk7.Value == "checked")
        sql += hdnLstProc7.Value + ",";

      if (hdnLstProcChk8.Value == "checked")
        sql += hdnLstProc8.Value + ",";

      if (hdnLstProcChk9.Value == "checked")
        sql += hdnLstProc9.Value + ",";

      if (hdnLstProcChk10.Value == "checked")
        sql += hdnLstProc10.Value + ",";

      if (hdnLstProcChk11.Value == "checked")
        sql += hdnLstProc11.Value + ",";

      if (hdnLstProcChk12.Value == "checked")
        sql += hdnLstProc12.Value + ",";

      if (hdnLstProcChk13.Value == "checked")
        sql += hdnLstProc13.Value + ",";

      if (hdnLstProcChk14.Value == "checked")
        sql += hdnLstProc14.Value + ",";

      if (hdnLstProcChk15.Value == "checked")
        sql += hdnLstProc15.Value + ",";

      if (hdnLstProcChk16.Value == "checked")
        sql += hdnLstProc16.Value + ",";

      if (hdnLstProcChk17.Value == "checked")
        sql += hdnLstProc17.Value + ",";

      if (hdnLstProcChk18.Value == "checked")
        sql += hdnLstProc18.Value + ",";

      if (hdnLstProcChk19.Value == "checked")
        sql += hdnLstProc19.Value + ",";

      if (hdnLstProcChk20.Value == "checked")
        sql += hdnLstProc20.Value + ",";

      if (hdnLstProcChk21.Value == "checked")
        sql += hdnLstProc21.Value + ",";

      sql = sql.Substring(0, sql.Length - 1);     //remove the last comma 
      sql += ")";


      return sql;
    }


    private string CheckOptions(string sOpen, string sClosed, string sOlder60Days)
    {
      //the RCA Type selected options 

      string sql = "";

      //check for RCA status
      byte cnt = 0;

      sql += " and (upper(E.FIVEWHYSTATUS) <> 'DELETED') ";

      if (sClosed == "checked")
      {
        sql += " and (upper(E.FIVEWHYSTATUS) = 'CLOSED'";
        cnt++;
      }

      if (sOpen == "checked")
      {
        if (cnt > 0)
          sql += " or ( (upper(E.FIVEWHYSTATUS) <> 'CLOSED') and (upper(E.FIVEWHYSTATUS) <> 'NOT REQUIRED') or (upper(E.FIVEWHYSTATUS) = 'TO BE RAISED') )";

        else
          sql += " and ( (upper(E.FIVEWHYSTATUS) <> 'CLOSED') and (upper(E.FIVEWHYSTATUS) <> 'NOT REQUIRED')  or (upper(E.FIVEWHYSTATUS) = 'TO BE RAISED') ) ";
      }

      if (cnt > 0)
        sql += ")";


      //check for > 60 days not yet closed
      if (sOlder60Days == "checked")
      {
        //sql += " and ( (upper(rca.status) <> 'CLOSED' and (upper(rca.status) <> 'NOT REQUIRED') or (upper(E.FIVEWHYSTATUS) = 'TO BE RAISED') )"
        //  + " and (TO_DATE(sysdate, 'DD-MM-YYYY') - TO_DATE(DateFrom, 'DD-MM-YYYY HH24:MI:SS')) > 60)";
        sql += " and ( (upper(E.FIVEWHYSTATUS) <> 'CLOSED' and (upper(E.FIVEWHYSTATUS) <> 'NOT REQUIRED') or (upper(E.FIVEWHYSTATUS) = 'TO BE RAISED') )"
          + " and ( ( sysdate - DateFrom) > 60) )";
      }

      return sql;
    }


    private void DoReport()
    {


      string stmp = "";

      try
      {
        foreach (DataRow item in grid.Rows)
        {
          TableCell cRegion = new TableCell();
          TableCell cSite = new TableCell();
          TableCell cPAG = new TableCell();
          TableCell cPA = new TableCell();
          TableCell cEvent = new TableCell();
          TableCell cStatus = new TableCell();
          TableCell cdtHrs = new TableCell();
          TableCell cTotActionItems = new TableCell();
          TableCell cTotActionItemsOpen = new TableCell();
          TableCell cDurationOpen = new TableCell();
          TableCell cRCADesc = new TableCell();
          TableCell cManager = new TableCell();

          cRegion.CssClass = "tableBorder nowrapStyle";
          cSite.CssClass = "tableBorder nowrapStyle";
          cPAG.CssClass = "tableBorder nowrapStyle";
          cPA.CssClass = "tableBorder nowrapStyle";
          cEvent.CssClass = "tableBorder nowrapStyle";
          cStatus.CssClass = "tableBorder nowrapStyle";
          cdtHrs.CssClass = "tableBorder nowrapStyle";
          cTotActionItems.CssClass = "tableBorder nowrapStyle";
          cTotActionItemsOpen.CssClass = "tableBorder nowrapStyle";
          cDurationOpen.CssClass = "tableBorder nowrapStyle";
          cRCADesc.CssClass = "tableBorder nowrapStyle";
          cManager.CssClass = "tableBorder nowrapStyle";

          cRegion.HorizontalAlign = HorizontalAlign.Center;
          cSite.HorizontalAlign = HorizontalAlign.Center;
          cPAG.HorizontalAlign = HorizontalAlign.Left;
          cPA.HorizontalAlign = HorizontalAlign.Left;
          cEvent.HorizontalAlign = HorizontalAlign.Center;
          cStatus.HorizontalAlign = HorizontalAlign.Left;
          cdtHrs.HorizontalAlign = HorizontalAlign.Center;
          cTotActionItems.HorizontalAlign = HorizontalAlign.Center;
          cTotActionItemsOpen.HorizontalAlign = HorizontalAlign.Center;
          cDurationOpen.HorizontalAlign = HorizontalAlign.Center;
          cRCADesc.HorizontalAlign = HorizontalAlign.Left;
          cManager.HorizontalAlign = HorizontalAlign.Center;



          cRegion.Text = item["region"].ToString();
          cSite.Text = item["site"].ToString();
          cPAG.Text = item["busprocname"].ToString();
          cPA.Text = item["FunctionalDesc"].ToString();

          if (item["site"].ToString() == cFG.NGX)
          {
            if (string.IsNullOrEmpty(item["EventID"].ToString()) == false)
              cEvent.Text = "<a href=\"http://ngxviis03:83/OEENet/frmRCALand.aspx?EVENTID=" + item["EventID"].ToString() + "\" target=\"_blank\">" + item["EventID"].ToString() + "</a>";
            else
              cEvent.Text = "<a href=\"http://ngxviis03:83/OEENet/frmRCALand.aspx?EVENTID=" + item["EventNum"].ToString() + "\" target=\"_blank\">" + item["EventNum"].ToString() + "</a>";
            //lblRCASysTriggersNGX.Text =  item["TotRCATOBeRaised"].ToString();
            //lblRCAInitiatedNGX.Text = item["TotRCAInitiated"].ToString();
            //lblRCAInProgressNGX.Text = item["TotRCAInProgress"].ToString();
            //lblRCANotRequiredNGX.Text =  item["TotRCANotRequired"].ToString();
            //lblRCAClosedNGX.Text = item["TotRCAClosed"].ToString();
            //lblRCAExpectedNGX.Text = (math.geti(item["TotRCATOBeRaised"].ToString()) + math.geti(item["TotRCAInitiated"].ToString())
            //      + math.geti(item["TotRCAInProgress"].ToString()) + math.geti(item["TotRCAClosed"].ToString())).ToString();

            //lblTotActionItemsNGX.Text = item["TotActionItems"].ToString();
            //lblTotActionItemsClosedNGX.Text = item["TotActionItemsClosed"].ToString();
          }

          if (item["site"].ToString() == cFG.SAI)
          {
            if (string.IsNullOrEmpty(item["EventID"].ToString()) == false)
              cEvent.Text = "<a href=\"http://saivls01:82/OEE/frmRCALand.aspx?EVENTID=" + item["EventID"].ToString() + "\" target=\"_blank\">" + item["EventID"].ToString() + "</a>";
            else
              cEvent.Text = "<a href=\"http://saivls01:82/OEE/frmRCALand.aspx?EVENTID=" + item["EventNum"].ToString() + "\" target=\"_blank\">" + item["EventNum"].ToString() + "</a>";

            //lblRCASysTriggersSAI.Text = item["TotRCATOBeRaised"].ToString();
            //lblRCAInitiatedSAI.Text = item["TotRCAInitiated"].ToString();
            //lblRCAInProgressSAI.Text = item["TotRCAInProgress"].ToString();
            //lblRCANotRequiredSAI.Text = item["TotRCANotRequired"].ToString();
            //lblRCAClosedSAI.Text =   item["TotRCAClosed"].ToString();
            //lblRCAExpectedSAI.Text = (math.geti(item["TotRCATOBeRaised"].ToString()) + math.geti(item["TotRCAInitiated"].ToString())
            //      + math.geti(item["TotRCAInProgress"].ToString()) + math.geti(item["TotRCAClosed"].ToString())).ToString();

            //lblTotActionItemsSAI.Text = item["TotActionItems"].ToString();
            //lblTotActionItemsClosedSAI.Text = item["TotActionItemsClosed"].ToString();
          }

          if (item["site"].ToString() == cFG.STA)
          {
            if (string.IsNullOrEmpty(item["EventID"].ToString()) == false)
              cEvent.Text = "<a href=\"http://stavls02/oee/frmRCALand.aspx?EVENTID=" + item["EventID"].ToString() + "\" target=\"_blank\">" + item["EventID"].ToString() + "</a>";
            else
              cEvent.Text = "<a href=\"http://stavls02/oee/frmRCALand.aspx?EVENTID=" + item["EventNum"].ToString() + "\" target=\"_blank\">" + item["EventNum"].ToString() + "</a>";

            //lblRCASysTriggersSTA.Text = item["TotRCATOBeRaised"].ToString();
            //lblRCAInitiatedSTA.Text = item["TotRCAInitiated"].ToString();
            //lblRCAInProgressSTA.Text = item["TotRCAInProgress"].ToString();
            //lblRCANotRequiredSTA.Text = item["TotRCANotRequired"].ToString();
            //lblRCAClosedSTA.Text = item["TotRCAClosed"].ToString();
            //lblRCAExpectedSTA.Text = (math.geti(item["TotRCATOBeRaised"].ToString()) + math.geti(item["TotRCAInitiated"].ToString())
            //                  + math.geti(item["TotRCAInProgress"].ToString()) + math.geti(item["TotRCAClosed"].ToString())).ToString();
            //lblTotActionItemsSTA.Text = item["TotActionItems"].ToString();
            //lblTotActionItemsClosedSTA.Text = item["TotActionItemsClosed"].ToString();
          }

          if (item["site"].ToString() == cFG.TUG)
          {
            if (string.IsNullOrEmpty(item["EventID"].ToString()) == false)
              cEvent.Text = "<a href=\"http://tugvls17/oeenet/frmRCALand.aspx?EVENTID=" + item["EventID"].ToString() + "\" target=\"_blank\">" + item["EventID"].ToString() + "</a>";
            else
              cEvent.Text = "<a href=\"http://tugvls17/oeenet/frmRCALand.aspx?EVENTID=" + item["EventNum"].ToString() + "\" target=\"_blank\">" + item["EventNum"].ToString() + "</a>";


            //lblRCASysTriggersTUG.Text = item["TotRCATOBeRaised"].ToString();
            //lblRCAInitiatedTUG.Text = item["TotRCAInitiated"].ToString();
            //lblRCAInProgressTUG.Text = item["TotRCAInProgress"].ToString();
            //lblRCANotRequiredTUG.Text = item["TotRCANotRequired"].ToString();
            //lblRCAClosedTUG.Text = item["TotRCAClosed"].ToString();
            //lblRCAExpectedTUG.Text = (math.geti(item["TotRCATOBeRaised"].ToString()) + math.geti(item["TotRCAInitiated"].ToString())
            //                  + math.geti(item["TotRCAInProgress"].ToString()) + math.geti(item["TotRCAClosed"].ToString())).ToString();

            //lblTotActionItemsTUG.Text = item["TotActionItems"].ToString();
            //lblTotActionItemsClosedTUG.Text = item["TotActionItemsClosed"].ToString();
          }

          if (string.IsNullOrEmpty(item["Status"].ToString()) == false)
            cStatus.Text = item["Status"].ToString();
          else
            cStatus.Text = math.CapWords(item["FIVEWHYSTATUS"].ToString());

          cdtHrs.Text = math.CapWords(item["dtHrs"].ToString());
          cTotActionItems.Text = item["ActionItemsTot"].ToString();
          cTotActionItemsOpen.Text =  item["ActionItemsOpen"].ToString();
          cDurationOpen.Text =  item["DurationOpenInDays"].ToString();

          stmp = item["problemdesc"].ToString();
          cRCADesc.Text = math.GetStr(ref stmp, 45);        // to fit column

          cManager.Text =  item["Manager"].ToString();

          TableRow R = new TableRow();
          R.CssClass = "tableBorder bodyText";
          R.BackColor = System.Drawing.Color.White;
          //R.HorizontalAlign = HorizontalAlign.Center;
          
          R.Cells.Add(cRegion);
          R.Cells.Add(cSite);
          R.Cells.Add(cPAG);
          R.Cells.Add(cPA);
          R.Cells.Add(cEvent);
          R.Cells.Add(cStatus);
          R.Cells.Add(cdtHrs);
          R.Cells.Add(cTotActionItems);
          R.Cells.Add(cTotActionItemsOpen);
          R.Cells.Add(cDurationOpen);
          R.Cells.Add(cRCADesc);
          R.Cells.Add(cManager);

          tblItems.Rows.Add(R);

        } //loop grid

      }
      catch (Exception er)
      {
        lblMsg.Text = er.Message;
      }


    } //


    private void AddToCboProcArea(string pStr)
    {
      //append the dropdown
      ListItem lst = cboProcessArea.Items.FindByText(pStr);
      if (lst == null)
        cboProcessArea.Items.Add(new ListItem(pStr, pStr));
    } //


    private void AddToCboRCAStatus(string pStr)
    {
      //append the dropdown
      ListItem lst = cboRCAStatus.Items.FindByText(pStr);
      if (lst == null)
        cboRCAStatus.Items.Add(new ListItem(pStr, pStr));
    } //


    private void DoReportTotals()
    {

      lblRCASysTriggersNGX.Text = "";
      lblRCAInitiatedNGX.Text = "";
      lblRCAInProgressNGX.Text = "";
      lblRCANotRequiredNGX.Text = "";
      lblRCAClosedNGX.Text = "";
      lblRCAExpectedNGX.Text = "";
      lblTotActionItemsNGX.Text = "";
      lblTotActionItemsClosedNGX.Text = "";
      lblDurationOpenNGX.Text = "";


      lblRCASysTriggersSAI.Text = "";
      lblRCAInitiatedSAI.Text = "";
      lblRCAInProgressSAI.Text = "";
      lblRCANotRequiredSAI.Text = "";
      lblRCAClosedSAI.Text = "";
      lblRCAExpectedSAI.Text = "";
      lblTotActionItemsSAI.Text = "";
      lblTotActionItemsClosedSAI.Text = "";
      lblDurationOpenSAI.Text = "";


      lblRCASysTriggersSTA.Text = "";
      lblRCAInitiatedSTA.Text = "";
      lblRCAInProgressSTA.Text = "";
      lblRCANotRequiredSTA.Text = "";
      lblRCAClosedSTA.Text = "";
      lblRCAExpectedSTA.Text = "";
      lblTotActionItemsSTA.Text = "";
      lblTotActionItemsClosedSTA.Text = "";
      lblDurationOpenSTA.Text = "";

      lblRCASysTriggersTUG.Text = "";
      lblRCAInitiatedTUG.Text = "";
      lblRCAInProgressTUG.Text = "";
      lblRCANotRequiredTUG.Text = "";
      lblRCAClosedTUG.Text = "";
      lblRCAExpectedTUG.Text = "";
      lblTotActionItemsTUG.Text = "";
      lblTotActionItemsClosedTUG.Text = "";
      lblDurationOpenTUG.Text = "";


      try
      {
        foreach (DataRow item in dtTot.Rows)
        {

          if (item["site"].ToString() == cFG.NGX)
          {

            lblRCASysTriggersNGX.Text = item["TotRCATOBeRaised"].ToString();
            lblRCAInitiatedNGX.Text = item["TotRCAInitiated"].ToString();
            lblRCAInProgressNGX.Text = item["TotRCAInProgress"].ToString();
            lblRCANotRequiredNGX.Text = item["TotRCANotRequired"].ToString();
            lblRCAClosedNGX.Text = item["TotRCAClosed"].ToString();
            lblRCAExpectedNGX.Text = (math.geti(item["TotRCATOBeRaised"].ToString()) + math.geti(item["TotRCAInitiated"].ToString())
                  + math.geti(item["TotRCAInProgress"].ToString()) + math.geti(item["TotRCAClosed"].ToString())).ToString();

            lblTotActionItemsNGX.Text = item["TotActionItems"].ToString();
            lblTotActionItemsClosedNGX.Text = item["TotActionItemsClosed"].ToString();
            lblDurationOpenNGX.Text = item["AveDurationRCAOpen"].ToString();
            
          }

          if (item["site"].ToString() == cFG.SAI)
          {

            lblRCASysTriggersSAI.Text = item["TotRCATOBeRaised"].ToString();
            lblRCAInitiatedSAI.Text = item["TotRCAInitiated"].ToString();
            lblRCAInProgressSAI.Text = item["TotRCAInProgress"].ToString();
            lblRCANotRequiredSAI.Text = item["TotRCANotRequired"].ToString();
            lblRCAClosedSAI.Text = item["TotRCAClosed"].ToString();
            lblRCAExpectedSAI.Text = (math.geti(item["TotRCATOBeRaised"].ToString()) + math.geti(item["TotRCAInitiated"].ToString())
                  + math.geti(item["TotRCAInProgress"].ToString()) + math.geti(item["TotRCAClosed"].ToString())).ToString();

            lblTotActionItemsSAI.Text = item["TotActionItems"].ToString();
            lblTotActionItemsClosedSAI.Text = item["TotActionItemsClosed"].ToString();
            lblDurationOpenSAI.Text = item["AveDurationRCAOpen"].ToString();
          }

          if (item["site"].ToString() == cFG.STA)
          {

            lblRCASysTriggersSTA.Text = item["TotRCATOBeRaised"].ToString();
            lblRCAInitiatedSTA.Text = item["TotRCAInitiated"].ToString();
            lblRCAInProgressSTA.Text = item["TotRCAInProgress"].ToString();
            lblRCANotRequiredSTA.Text = item["TotRCANotRequired"].ToString();
            lblRCAClosedSTA.Text = item["TotRCAClosed"].ToString();
            lblRCAExpectedSTA.Text = (math.geti(item["TotRCATOBeRaised"].ToString()) + math.geti(item["TotRCAInitiated"].ToString())
                              + math.geti(item["TotRCAInProgress"].ToString()) + math.geti(item["TotRCAClosed"].ToString())).ToString();
            lblTotActionItemsSTA.Text = item["TotActionItems"].ToString();
            lblTotActionItemsClosedSTA.Text = item["TotActionItemsClosed"].ToString();
            lblDurationOpenSTA.Text = item["AveDurationRCAOpen"].ToString();
          }

          if (item["site"].ToString() == cFG.TUG)
          {

            lblRCASysTriggersTUG.Text = item["TotRCATOBeRaised"].ToString();
            lblRCAInitiatedTUG.Text = item["TotRCAInitiated"].ToString();
            lblRCAInProgressTUG.Text = item["TotRCAInProgress"].ToString();
            lblRCANotRequiredTUG.Text = item["TotRCANotRequired"].ToString();
            lblRCAClosedTUG.Text = item["TotRCAClosed"].ToString();
            lblRCAExpectedTUG.Text = (math.geti(item["TotRCATOBeRaised"].ToString()) + math.geti(item["TotRCAInitiated"].ToString())
                              + math.geti(item["TotRCAInProgress"].ToString()) + math.geti(item["TotRCAClosed"].ToString())).ToString();

            lblTotActionItemsTUG.Text = item["TotActionItems"].ToString();
            lblTotActionItemsClosedTUG.Text = item["TotActionItemsClosed"].ToString();
            lblDurationOpenTUG.Text = item["AveDurationRCAOpen"].ToString();
          }


        }

      }
      catch (Exception er)
      {
        lblMsg.Text = er.Message;
      }


    } //ReportTotals


    private void RefreshPage()
    {

      Response.Redirect(Request.RawUrl, true);

      //Response.CacheControl = "no-cache";
      //Response.AddHeader("Pragma", "no-cache");
      //Response.Expires = -1;
      //int y = 0;
      ////Response.Write("<script type='text/javascript'>locaton.reload()</script>");

      //Uri uri = Request.Url;

      //Uri uri2 = Request.UrlReferrer;

      //Response.Redirect(Request.RawUrl + "?" + DateTime.Now.Ticks.ToString(), true);

    }



    protected void cboProcessArea_SelectedIndexChanged(object sender, EventArgs e)
    {//dropdown changed
      if (cboProcessArea.SelectedIndex != 0)
          LoadData(cboProcessArea.SelectedItem.Text,"", "");
    }



    protected void cboRCAStatus_SelectedIndexChanged(object sender, EventArgs e)
    {//dropdown changed
      if (cboRCAStatus.SelectedIndex != 0)
        LoadData("", cboRCAStatus.SelectedItem.Text,"");
    }

    protected void cmdDown_Click(object sender, EventArgs e)
    {
      LoadData("", "", "DESC");
    }

    protected void cmdUp_Click(object sender, EventArgs e)
    {
      LoadData("", "", "ASC");
    }








    //private void GetForm()
    //{

    //   if (Request.Form.Keys.Count > 0)
    //   {
    //      NameValueCollection coll = null;
    //      coll = Request.Form;

    //      string ss = "";

    //      // Get names of all forms into a string array.
    //      String[] arr1 = coll.AllKeys;
    //      for (int loop1 = 0; loop1 < arr1.Length; loop1++)
    //      {
    //         ss += arr1[loop1] + "<br / >";
    //      }
    //   }
    //}



  }///
}///




/*
      try
      {
        cORADB.OpenDB();
      }
      catch(Exception er)
      {
         lblMsg.Text = er.Message;
      }

      finally
      {
        cORADB.CloseDB();
      }

*/



/*
        try
      {

      }
      catch(Exception er)
      {
         lblMsg.Text = er.Message;
      }

      finally
      {

      }

*/

//private void GetForm()
//{

//  if (Request.Form.Keys.Count > 0)
//  {
//    NameValueCollection coll = null;
//    coll = Request.Form;

//    string ss = "";

//    // Get names of all forms into a string array.
//    String[] arr1 = coll.AllKeys;
//    for (int loop1 = 0; loop1 < arr1.Length; loop1++)
//    {
//      ss += arr1[loop1] + "<br / >";
//    }
//  }
// }

//} //

