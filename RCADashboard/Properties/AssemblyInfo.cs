﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("RCADashboard")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("RCADashboard")]
[assembly: AssemblyCopyright("Copyright ©  2020")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("5831f558-7c7f-4861-8ee4-8a7e34ce363b")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.0.5")]
[assembly: AssemblyFileVersion("1.0.0.5")]

//1.0.0.2 Updaed links to new RCA  frmRCALand.aspx
//2020-07-22 Changed to load all RCA totals on startup (All sites etc for entire period)
//and then when a date range is selected, populate the RCA Totals for the date range
//1.0.0.3 2022-07-19 C-2206-0188 OEE RCA Changes required to the MDT Investigation, Analysis,  Action Plan and RCA Finder
//1.0.0.4 2022-08-16 Added RCA Finder button links  for each Mill
//1.0.0.5 2024-03-06 Added RCA Detective button on main screen




