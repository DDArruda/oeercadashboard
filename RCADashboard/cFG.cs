﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Data;
using System.Collections;
using Oracle.ManagedDataAccess.Client;
using System.Configuration;
using System.Web.Configuration;
using System.DirectoryServices;
using System.IO;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Text;


namespace RCADashboard
{
  //CONFIG  CLASS

  public class cFG
  {
    public const byte NGODWANA = 1;
    public const byte STANGER = 2;
    public const byte TUGELA = 4;
    public const byte SAICCOR = 6;

    public const string NGX = "NGX";
    public const string SAI = "SAI";
    public const string STA = "STA";
    public const string TUG = "TUG";

    public const sbyte CAT_UNPLANNED = 0;
    public const sbyte CAT_PLANNED = 1;
    public const sbyte CAT_COMMENT = 2;
    public const sbyte CAT_RUNRATE = -1;



    public enum DatePeriods
    {
      eDay = 0,
      eWeek = 1,
      eMonth = 2,
      eQuarter = 3,
      eYear = 4,
      eManual = 5
    }


    public static string GetCurrentIdentity()
    {
      // must exist in web.config
      // <authentication mode="Windows">
      // </authentication>
      //BaseConfig.sUserName = Page.User.Identity.Name.Replace("SAPPISA\\", "");
      //BaseConfig.sUserName = Thread.CurrentPrincipal.Identity.Name.Replace("SAPPISA\\", "");
      //BaseConfig.sUserName = Request.ServerVariables["LOGON_USER"].Replace("SAPPISA\\", "");
      string sUser = HttpContext.Current.User.Identity.Name.Replace("SAPPISA\\", "");

      sUser = sUser.ToLower().Replace("adm-", string.Empty);

      if (sUser.ToUpper() == "DDEARRUDA")
        sUser = "DArruda";

      //sUser = "SNxele";

      return sUser;
    }


    public static bool SystemMaintenance()
    {
      try
      {
        string rv = ConfigurationManager.AppSettings["SystemMaintenance"].ToString();
        return (rv == "true");
      }
      catch (Exception err)
      {
        LogIt(err.Message);
        return false;
      }
    }



    public static string GetFolderLocation()
    {
      try
      {
        string rv = ConfigurationManager.AppSettings["Folder"].ToString();
        return rv;
      }
      catch (Exception err)
      {
        LogIt(err.Message);
        return "";
      }
    }



    public static string GetRCAServer()
    {
      string rv = ConfigurationManager.AppSettings["RCAServer"].ToString();
      return rv;
    }



    public static bool ApplyDecimalPoint()
    {
      try
      {
        string rv = ConfigurationManager.AppSettings["ApplyDecimalPoint"].ToString();
        return (rv == "true");
      }
      catch (Exception err)
      {
        LogIt(err.Message);
        return false;
      }
    }



    public static string GetPlanUnplanDesc(sbyte opt)
    {
      string rv = "";
      switch (opt)
      {
        case CAT_UNPLANNED: rv = "UNPLANNED"; break;
        case CAT_PLANNED: rv = "PLANNED"; break;
        case CAT_COMMENT: rv = "COMMENT"; break;
        case CAT_RUNRATE: rv = "RUNRATE"; break;
      }
      return rv;
    }//



    public static int GetPlanUnplanID(string descr)
    {
      int item = 0;

      switch (descr.ToUpper())
      {
        case "RUNRATE":
          item = CAT_RUNRATE;
          break;
        case "UNPLANNED":
          item = CAT_UNPLANNED;
          break;
        case "PLANNED":
          item = CAT_PLANNED;
          break;
        case "COMMENTS":
          item = CAT_COMMENT;
          break;
      }
      return item;

    }




    public static string getMonth(int iMonth)
    {
      string rv = "";
      switch (iMonth)
      {
        case 1: rv = "Jan"; break;
        case 2: rv = "Feb"; break;
        case 3: rv = "Mar"; break;
        case 4: rv = "Apr"; break;
        case 5: rv = "May"; break;
        case 6: rv = "Jun"; break;
        case 7: rv = "Jul"; break;
        case 8: rv = "Aug"; break;
        case 9: rv = "Sep"; break;
        case 10: rv = "Oct"; break;
        case 11: rv = "Nov"; break;
        case 12: rv = "Dec"; break;
      }

      return rv;

    }


    public static string getMonthSappi(int iMonth)
    {
      string rv = "";
      switch (iMonth)
      {
        case 1: rv = "Oct"; break;
        case 2: rv = "Nov"; break;
        case 3: rv = "Dec"; break;
        case 4: rv = "Jan"; break;
        case 5: rv = "Feb"; break;
        case 6: rv = "Mar"; break;
        case 7: rv = "Apr"; break;
        case 8: rv = "May"; break;
        case 9: rv = "Jun"; break;
        case 10: rv = "Jul"; break;
        case 11: rv = "Aug"; break;
        case 12: rv = "Sep"; break;
      }

      return rv;

    }



    public static int GetSiteID()
    {
      int rv = 0;

      string siteName = ConfigurationManager.AppSettings["Site"].ToString();

      switch (siteName)
      {
        case "Ngodwana":     rv = 1; break;
        case "Stanger":      rv = 2; break;
        case "CapeKraft":    rv = 3; break;
        case "Tugela":       rv = 4; break;
        case "Enstra":       rv = 5; break;
        case "Saiccor":      rv = 6; break;
        default:
          rv = 0; break;
      }

      return rv;
    }



    public static string GetSiteCode(byte siteID)
    {
      string rv = "";

      switch (siteID)
      {
        case 1:
          rv = NGX; break;
        case 2:
          rv = STA; break;
        case 4:
          rv = TUG; break;
        case 6:
          rv = SAI; break;
      }

      return rv;
    }


    public static bool isExec(string sLevel)
    {
      return (sLevel.ToUpper() == "E");
    }


    public static bool isAdmin(string sLevel)
    {
      return (sLevel.ToUpper() == "A");
    }
    public static bool isExecAdmin(string sLevel)
    {
      return ((sLevel.ToUpper() == "E") || (sLevel.ToUpper() == "A"));
    }
    public static bool isSuper(string sLevel)
    {
      return (sLevel.ToUpper() == "S");
    }
    public static bool isOper(string sLevel)
    {
      return (sLevel.ToUpper() == "O");
    }
    public static bool is5Why(string sLevel)
    {
      return (sLevel.ToUpper() == "5");
    }
    public static bool isReadOnly(string sLevel)
    {
      return (sLevel.ToUpper() == "R");
    }

    public static bool IsFinance(string sLevel)
    {
      return (sLevel.ToUpper() == "F");
    }

    public static bool IsExecFin(string sLevel)
    {
      return ((sLevel.ToUpper() == "E") || (sLevel.ToUpper() == "F"));
    }


    public static bool IsExecFinAdmin(string sLevel)
    {
      return ((sLevel.ToUpper() == "E") || (sLevel.ToUpper() == "F") || (sLevel.ToUpper() == "A"));

    }




    /// <summary>
    /// Read application settings from Web.config Tag <appSettings>
    /// </summary>
    /// <param name="skey"></param>
    ///<returns></returns>
    public static string ReadAppSettings(string skey)
    {
      AppSettingsReader appReader = new AppSettingsReader();
      try
      {
        return (string)appReader.GetValue(skey, typeof(System.String));
      }
      catch (System.Exception e)
      {
        return e.Message.ToString();
      }
    }




    /// <summary>
    /// GET THE RADIO BUTTON SELECTED PERIOD AS A STRING
    /// </summary>
    /// <param name="radioButton"></param>
    /// <returns>DAY-WEEK-MONTH-QUARTER-YEAR</returns>
    public static string GetSelPeriod(int radioButton)
    {
      string repPeriod = "";
      switch (radioButton)
      {
        case 0:
          repPeriod = "DAY";
          break;
        case 1:
          repPeriod = "WEEK";
          break;
        case 2:
          repPeriod = "MONTH";
          break;
        case 3:
          repPeriod = "QUARTER";
          break;
        case 4:
          repPeriod = "YEAR";
          break;
        case 5:     // NOT A RADION BUTTON, USED FOR DATE FROM - TO RANGE, NO SAPPI DATES
          repPeriod = "MANUAL";
          break;
      }
      return repPeriod;
    }




    public static void LogIt(string str)
    {
      //string sPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
      string sPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments);
      try
      {
        using (StreamWriter wr = new StreamWriter(sPath + "\\zDashBoardLog.log", true))
        {
          wr.WriteLine(DateTime.Now + " " + str);
          wr.Close();
        }
      }
      catch (Exception e)
      {
        sPath = e.Message;
      }
      finally
      {
        sPath = "";
      }
    }


    public static void LogUser(string sFile, string str)
    {
      try
      {
        using (StreamWriter wr = new StreamWriter(sFile, true))
        {
          wr.WriteLine(DateTime.Now.ToShortTimeString() + " " + str);
          wr.Close();
        }
      }
      catch (Exception e)
      {
        LogIt("[LogUser] " + e.Message);
      }
      finally
      {
      }
    }


  }///
}///