﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.DirectoryServices;
using System.IO;


namespace RCADashboard
{

  public class cAD
  {

    public string strEmpName = "";
    public string strEmpNo = "";
    public string strEmail = "";
    public string strDep = "";
    public string strManager = "";
    public string strTitle = "";
    public string strTelNo = "";
    public string strCompany = "";
    public string strMill = "";
    public string strUserName = "";
    public string strError = "";

    public void Clear()
    {
      strEmpName = "";
      strEmpNo = "";
      strEmail = "";
      strDep = "";
      strManager = "";
      strTitle = "";
      strTelNo = "";
      strCompany = "";
      strMill = "";
      strUserName = "";
      strError = "";
    }



    public bool InActiveDir(string strUserName, ref string empid)
    {
      bool bStatus = false;
      short myLoop = 0;
      string sDomain = "";


      int hasDomain = strUserName.IndexOf(@"\");

      if (hasDomain > 0) strUserName = strUserName.Remove(0, hasDomain + 1);
      try
      {
        string sDCIP = cFG.ReadAppSettings("DC_IP");
        while (myLoop < 3)
        {
          if (myLoop == 0)
            sDomain = "ZA";

          if (myLoop == 1)
            sDomain = "NA";

          if (myLoop == 2)
            sDomain = "EU";

          myLoop++;

          DirectoryEntry adfolderobject = new DirectoryEntry("LDAP://" + sDCIP + "/DC=" + sDomain + ",DC=SAPPI,DC=com");
          DirectorySearcher adsearchobject = new DirectorySearcher(adfolderobject);
          adsearchobject.SearchScope = SearchScope.Subtree;
          adsearchobject.PageSize = 5000;
          adsearchobject.Filter = "(&(objectCategory=person)(objectClass=user)(samaccountname=" + strUserName + ")(!userAccountControl:1.2.840.113556.1.4.803:=2)(!samAccountName=adm-*)(!samAccountName=sys-*)(!samAccountName=sys*-*))";
          foreach (SearchResult searchResult in adsearchobject.FindAll())
          {
            if (searchResult.Properties.Contains("samaccountname"))
              if (searchResult.Properties["samaccountname"][0].ToString().ToLower() == strUserName.ToLower())
              {
                bStatus = true;
                myLoop = 10;
              }

            if (searchResult.Properties.Contains("employeeid"))
            {
              empid = searchResult.Properties["employeeid"][0].ToString();
              myLoop = 10;
            }
          }
        }// while
      }  //TRY
      catch (Exception ex)
      {
        if (ex.Message.Replace("\r\n", "") == "Active Directory server not available.")
          bStatus = false; ;
      }
      return bStatus;

    }





    public bool GetUserDetails(string strUserName)
    {
      bool bStatus = false;
      short myLoop = 0;
      string sDomain = "";


      int hasDomain = strUserName.IndexOf(@"\");

      if (hasDomain > 0)
        strUserName = strUserName.Remove(0, hasDomain + 1);

      try
      {
        DirectoryEntry adfolderobject = null;

        string sDCIP = cFG.ReadAppSettings("DC_IP");


        while (myLoop < 3)
        {
          if (myLoop == 0)
            sDomain = "ZA";

          if (myLoop == 1)
            sDomain = "NA";

          if (myLoop == 2)
            sDomain = "EU";

          myLoop++;


          adfolderobject = new DirectoryEntry("LDAP://" + sDCIP + "/DC=" + sDomain + ",DC=SAPPI,DC=com");

          DirectorySearcher adsearchobject = new DirectorySearcher(adfolderobject);
          adsearchobject.SearchScope = SearchScope.Subtree;
          adsearchobject.PageSize = 5000;
          adsearchobject.Filter = "(&(objectCategory=person)(objectClass=user)(samaccountname=" + strUserName + ")(!userAccountControl:1.2.840.113556.1.4.803:=2)(!samAccountName=adm-*)(!samAccountName=sys-*)(!samAccountName=sys*-*))";
          foreach (SearchResult searchResult in adsearchobject.FindAll())
          {
            if (searchResult.Properties.Contains("samaccountname"))
            {

              if (searchResult.Properties["samaccountname"][0].ToString().ToLower() == strUserName.ToLower())
              {
                if (searchResult.Properties.Contains("displayname"))
                  strEmpName = searchResult.Properties["displayname"][0].ToString().Replace(",", "");

                if (searchResult.Properties.Contains("employeeid"))
                {
                  strEmpNo = Convert.ToDouble(searchResult.Properties["employeeid"][0]).ToString();
                }

                //---------------------------------------------------
                // get the Mill Name  ie Saiccor
                string tmp = searchResult.Path;

                int u = tmp.IndexOf("OU=");
                tmp = tmp.Substring(u);

                u = tmp.IndexOf("OU=");
                if (u >= 0)
                  strMill = math.SplitStr(ref tmp, ',');

                u = tmp.IndexOf("OU=");
                if (u >= 0)
                  strMill = math.SplitStr(ref tmp, ',');

                u = tmp.IndexOf("OU=");
                if (u >= 0)
                  strMill = math.SplitStr(ref tmp, ',');

                tmp = strMill;
                strMill = math.SplitStr(ref tmp, '=');
                strMill = math.SplitStr(ref tmp, '=');
                //---------------------------------------------------

                if (searchResult.Properties.Contains("mail"))
                  strEmail = searchResult.Properties["mail"][0].ToString();

                if (searchResult.Properties.Contains("department"))
                  strDep = searchResult.Properties["department"][0].ToString();

                if (searchResult.Properties.Contains("manager"))
                  strManager = searchResult.Properties["manager"][0].ToString();

                if (searchResult.Properties.Contains("title"))
                  strTitle = searchResult.Properties["title"][0].ToString();

                if (searchResult.Properties.Contains("company"))
                  strCompany = searchResult.Properties["company"][0].ToString();

                if (searchResult.Properties.Contains("telephonenumber"))
                  strTelNo = searchResult.Properties["telephonenumber"][0].ToString();

                //------------------
                if (searchResult.Properties.Contains("userprincipalname"))
                {
                  string strEmail2 = searchResult.Properties["mail"][0].ToString();

                  if (Convert.ToDouble(searchResult.Properties["employeeid"][0]) > 70000000)
                  {
                    string strEmpNo2 = searchResult.Properties["employeeid"][0].ToString().Remove(0, 2);
                  }
                  else
                  {
                    string strEmpNo2 = searchResult.Properties["employeeid"][0].ToString();
                  }

                  string strName2 = searchResult.Properties["name"][0].ToString();
                }
                //------------------

                bStatus = true;
                myLoop = 10;
              }
            }
          }  // FOR EACH

        }


      }  //TRY
      catch (Exception ex)
      {
        if (ex.Message.Replace("\r\n", "") == "Active Directory server not available.")
        {
          bStatus = true;
        }
      }

      return bStatus;
    }




  }///
}///