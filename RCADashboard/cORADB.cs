﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using Oracle.ManagedDataAccess.Client;


namespace RCADashboard
{
  public static class cORADB
  {
    private static OracleConnection con;
    private static OracleCommand cmd;
    private static string siteName;




    public static void CloseDB()
    {
      if (con == null)
        return;
      try
      {
        if (con.State != ConnectionState.Closed)
        {
          con.Close();
        }
      }
      catch (Exception)
      { }

      cmd = null;
      con = null;
      return;

    } //




    public static void OpenDB(string  site = "")
    {

      if (con?.State == ConnectionState.Open)
      {
        return;
      }

      if (site == "")
        siteName = ConfigurationManager.AppSettings["Site"].ToString();   //default site loaded
      else
        siteName = ConfigurationManager.AppSettings[site].ToString();     //other sites

      string conStr = ConfigurationManager.ConnectionStrings[siteName].ConnectionString;

      try
      {
        con = new OracleConnection(conStr);
        con.Open();
      }
      catch (Exception ee)
      {
        con = null;
        throw new Exception("Cannot connect to Database : " + ee.Message);
      }

    } //




    public static OracleParameter GetCursorParam()
    {
      OracleParameter cp = new OracleParameter
      {
        ParameterName = "pCursor",
        OracleDbType = OracleDbType.RefCursor,
        Direction = ParameterDirection.Output
      };
      return cp;
    }





    public static DataSet GetDataset(string sql)
    {
      try
      {
        DataSet ds = new DataSet();
        OracleDataAdapter da = new OracleDataAdapter(sql, con);
        da.Fill(ds);
        return ds;
      }
      catch (Exception e)
      {
        throw e;
      }
    }




    public static DataTable GetDataTable(string sql)
    {
      try
      {
        OracleDataAdapter da = new OracleDataAdapter(sql, con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        return dt;
      }
      catch (Exception e)
      {
        throw e;
      }
    }



    public static int ExecScalar(string sql)
    {
      int rv = 0;
      try
      {
        OracleCommand cmd = new OracleCommand(sql);
        cmd.Connection = con;
        object result = cmd.ExecuteScalar();
        rv = (int)result;
      }
      catch (Exception er)
      {
        cFG.LogIt("Exec Scalar " + er.Message);
        rv = 0;
      }

      return rv;

    } //




    public static void ExecQuery(string sql)
    {
      try
      {
        OracleCommand cmd = new OracleCommand(sql);
        cmd.Connection = con;
        int rs = cmd.ExecuteNonQuery();
      }
      catch (Exception ee)
      {
        throw ee;
      }
    }


    public static void addDT(string sql , DataTable dt)
    {
      try
      {
        OracleDataAdapter da = new OracleDataAdapter(sql,con);
        da.Fill(dt);
      }
      catch (Exception eish)
      {
        throw eish;
      }
    }




  } ///
}///