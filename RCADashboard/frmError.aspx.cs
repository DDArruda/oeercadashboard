﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RCADashboard
{
  public partial class frmError : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {

      try
      {
        if (Request.QueryString.Count > 0)
        {
          lblMsg.Text = Request.QueryString.Get("q");
        }
      }
      catch (Exception err)
      {
        cFG.LogIt(err.Message);
        //lblMsg.Text = "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium " +
        //"voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate " +
        //"non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum " +
        //"fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis ";
      }

    }


  }//
}//